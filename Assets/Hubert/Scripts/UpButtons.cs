﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UpButtons : MonoBehaviour {

    public int stats;
    public Player player;

	void Update ()
    {
        if(player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        }

        if (Input.GetButtonDown("LevelUp" + stats))
        {
            if(stats == 1)
            {
                healthUp();
            }
            else if (stats == 2)
            {
                damageUp();
            }
            else if (stats == 3)
            {
                magicUp();
            }
        }
	}

    public void healthUp()
    {
        if (player.playerStats.up > 0)
            player.playerStats.healthUp();
    }

    public void damageUp()
    {
        if (player.playerStats.up > 0)
            player.playerStats.strengthUp();
    }

    public void magicUp()
    {
        if (player.playerStats.up > 0)
            player.playerStats.magicUp();
    }
}
