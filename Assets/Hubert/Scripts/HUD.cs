﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HUD : MonoBehaviour {

    bool active = false;
    public Player player;
    public GameObject levelUpMenu;
    public GameObject levelUpMenuButton;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();        
    }

	void Update ()
    {
        LevelUp();

        if (Input.GetButtonDown("Menu1"))
        {
            DeactivateThis(levelUpMenuButton);
        }

        if (Input.GetButtonDown("MenuSubmit1") && levelUpMenuButton.activeInHierarchy)
        {
            DeactivateThis(levelUpMenu);
        }
        else if (!levelUpMenuButton.activeInHierarchy)
        {
            levelUpMenu.SetActive(false);
        }
	}

    void DeactivateThis(GameObject obj)
    {
        active = !active;
        obj.SetActive(active);
    }

    void LevelUp()
    {
        if (player.playerStats.up > 0)
        {
            levelUpMenuButton.GetComponent<Animator>().SetBool("LevelUp", true);
        }
        else
            levelUpMenuButton.GetComponent<Animator>().SetBool("LevelUp", false);
    }
}
