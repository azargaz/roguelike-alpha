﻿using UnityEngine;
using System.Collections;

public class Enemy : MoveObject
{

    [System.Serializable]
    public class EnemyStats
    {
        public float maxHealth = 10;
        public float curHealth = 0;

        public int maxSkillCooldown = 10;
        public int curSkillCooldown = 0;

        public float damage = 1;

        public void resetStats()
        {
            curHealth = maxHealth;
            curSkillCooldown = maxSkillCooldown;
        }
    }

    [Header("Stats")]
    public EnemyStats enemyStats = new EnemyStats();

    public enum enemyType { melee, ranged, mage, underground };

    [Header("Type")]
    [SerializeField]
    private enemyType type;    

    public enum direction { up, right, down, left };
    private direction dir;

    private Transform playerPos;
    private float timeToSearch = 0f;

    private int dirx = 0;
    private int diry = 0;

    private int distx = 0;
    private int disty = 0;

    public GameObject deathParticles;

    [Header("Drop")]
    public int[] experience = new int[4];
    public GameObject[] drops = new GameObject[1];

    [Header("Projectiles")]
    public GameObject projectile;
    public Transform shootingPoint;
    public float shootingForce;
    public ForceMode2D forceMode;
    public int Yrotation;

    private Player player;
    private ItemController itemController;
    private SpellController spellController;
    private BoardManager bmanager;

    [Header("Turns")]
    public int turnSkip;    // co ile tur może wykonać ruch
    public int skippedTurns = 0;
    public int runawayRange = 0;
    public int attackRange = 0;
    public bool playerInRange;
    public int stunned = 0;
    private bool burrow;

    IEnumerator FindPlayer()
    {
        playerPos = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        timeToSearch = Time.time + 0.3f;
        yield return null;
    }

    void Awake()
    {
        StartCoroutine(FindPlayer());
        Clock.turns.ListEnemies();

        playerInRange = false;
        burrow = false;
        enemyStats.resetStats();
        skippedTurns = turnSkip;

        bmanager = GameObject.Find("GameManager").GetComponent<BoardManager>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        itemController = GameObject.Find("GameManager").GetComponent<ItemController>(); //Kamil - potrzebne do itemów
        spellController = GameObject.Find("GameManager").GetComponent<SpellController>();
    }

    protected override void Start()
    {
        // funkcja Start z skryptu MoveObject
        base.Start();
    }

    void Update()
    {
        if (enemyStats.curHealth <= 0)
        {
            KillEnemy();
        }

        if (playerPos == null)
        {
            if (Time.time > timeToSearch)
                StartCoroutine(FindPlayer());
        }
        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        }

        if(stunned > 0)
        {
            sprite.GetComponent<SpriteRenderer>().color = new Vector4(0f, 0.5f, 1f, 0.5f);
        }
        else
        {
            sprite.GetComponent<SpriteRenderer>().color = new Vector4(1f, 1f, 1f, 1f);
        }
    }

    void BurrowUnburrow()
    {
        burrow = !burrow;

        if (burrow)
        {
            gameObject.layer = 14;
            anim.SetTrigger("Burrow");
        }
        else if (!burrow)
        {
            gameObject.layer = 11;
            anim.SetTrigger("Unburrow");
        }
    }

    protected override void AttemptMove<T>(int xDir, int yDir)
    {
        // funkcja AttemptMove z skryptu MoveObject
        base.AttemptMove<T>(xDir, yDir);
    }

    #region AI Kappa

    private void ChangeDirection(int i)
    {
        #region zmiana kierunku

        if (i == 0)
            dir = direction.up;
        else if (i == 1)
            dir = direction.right;
        else if (i == 2)
            dir = direction.down;
        else if (i == 3)
            dir = direction.left;

        switch (dir)
        {
            case (direction.up):
                dirx = 0;
                diry = 1;
                break;

            case (direction.right):
                dirx = 1;
                diry = 0;
                break;

            case (direction.down):
                dirx = 0;
                diry = -1;
                break;

            case (direction.left):
                dirx = -1;
                diry = 0;
                break;
        }

        return;

        #endregion
    }

    private void Pathfinding(int distx, int disty)
    {
        #region pathfinding

        // I ĆWIARTKA             
        if (distx > 0 && disty > 0)
        {
            // GÓRA
            if (disty > distx)
            {
                ChangeDirection(0);
            }
            // PRAWO
            else if (distx > disty)
            {
                ChangeDirection(1);
            }
            // X = Y
            else if (distx == disty)
            {
                if (Random.value > 0.5)
                    ChangeDirection(0);
                else
                    ChangeDirection(1);
            }
        }

        // II ĆWIARTKA
        if (distx > 0 && disty < 0)
        {
            // PRAWO
            if (distx > Mathf.Abs(disty))
            {
                ChangeDirection(1);
            }
            // DÓŁ
            else if (Mathf.Abs(disty) > distx)
            {
                ChangeDirection(2);
            }
            // X = |Y|
            else if (distx == Mathf.Abs(disty))
            {
                if (Random.value > 0.5)
                    ChangeDirection(1);
                else
                    ChangeDirection(2);
            }
        }

        // III ĆWIARTKA
        if (distx < 0 && disty < 0)
        {
            // DÓŁ
            if (Mathf.Abs(disty) > Mathf.Abs(distx))
            {
                ChangeDirection(2);
            }
            // LEWO
            else if (Mathf.Abs(distx) > Mathf.Abs(disty))
            {
                ChangeDirection(3);
            }
            // -X = -Y
            else if (distx == disty)
            {
                if (Random.value > 0.5)
                    ChangeDirection(2);
                else
                    ChangeDirection(3);
            }
        }

        // IV ĆWIARTKA 
        if (distx < 0 && disty > 0)
        {
            // GÓRA
            if (disty > Mathf.Abs(distx))
            {
                ChangeDirection(0);
            }
            // LEWO
            else if (Mathf.Abs(distx) > disty)
            {
                ChangeDirection(3);
            }
            // |X| = Y
            else if (Mathf.Abs(distx) == disty && distx < 0)
            {
                if (Random.value > 0.5)
                    ChangeDirection(0);
                else
                    ChangeDirection(3);
            }
        }

        // GÓRA / DÓŁ JEŚLI W LINII PROSTEJ DO GRACZA
        if (distx == 0)
        {
            if (disty > 0)
                ChangeDirection(0);
            else if (disty < 0)
                ChangeDirection(2);
        }

        // PRAWO / LEWO JEŚLI W LINII PROSTEJ DO GRACZA
        else if (disty == 0)
        {
            if (distx > 0)
                ChangeDirection(1);
            else if (distx < 0)
                ChangeDirection(3);
        }

        //Debug.Log("Pathfinding: " + dir);

        if (Mathf.Abs(distx) > 1 || Mathf.Abs(disty) > 1)
        {
            RaycastHit2D hit;

            Vector2 start = transform.position;
            Vector2 end = start + new Vector2(dirx, diry);

            bodyCollider.enabled = false;
            hit = Physics2D.Linecast(start, end, blockingLayer);
            bodyCollider.enabled = true;

            if (hit)
            {
                checkPath();
            }

            //Debug.Log("checkPath1: " + dir);

        }
        #endregion
    }


    private void checkPath()
    {
        #region sprawdzanie przeszkód na drodze

        // GÓRA ZABLOKOWANA
        if (dir == direction.up)
        {
            bool hit;

            Vector2 start = transform.position;
            Vector2 end = start + new Vector2(1f, 1f);

            if (bmanager.wallsPositions.Contains(end))
                hit = true;
            else
                hit = false;

            if (!hit)
            {
                ChangeDirection(1);
                return;
            }

            end = start + new Vector2(-1f, 1f);

            if (bmanager.wallsPositions.Contains(end))
                hit = true;
            else
                hit = false;

            if (!hit)
            {
                ChangeDirection(3);
                return;
            }

            if (distx > 0)
            {
                ChangeDirection(1);
            }
            else if (distx < 0)
            {
                ChangeDirection(3);
            }
            else if (distx == 0)
            {
                if (Random.value > 0.5)
                    ChangeDirection(1);
                else
                    ChangeDirection(3);
            }

            return;
        }

        // PRAWO ZABLOKOWANE
        if (dir == direction.right)
        {
            bool hit;

            Vector2 start = transform.position;
            Vector2 end = start + new Vector2(1f, 1f);

            if (bmanager.wallsPositions.Contains(end))
                hit = true;
            else
                hit = false;

            if (!hit)
            {
                ChangeDirection(0);
                return;
            }

            end = start + new Vector2(1f, -1f);

            if (bmanager.wallsPositions.Contains(end))
                hit = true;
            else
                hit = false;

            if (!hit)
            {
                ChangeDirection(2);
                return;
            }

            if (disty > 0)
            {
                ChangeDirection(0);
            }
            else if (disty < 0)
            {
                ChangeDirection(2);
            }
            else if (disty == 0)
            {
                if (Random.value > 0.5)
                    ChangeDirection(0);
                else
                    ChangeDirection(2);
            }
            return;
        }

        // DÓŁ ZABLOKOWANY
        if (dir == direction.down)
        {
            bool hit;

            Vector2 start = transform.position;
            Vector2 end = start + new Vector2(1f, -1f);

            if (bmanager.wallsPositions.Contains(end))
                hit = true;
            else
                hit = false;

            if (!hit)
            {
                ChangeDirection(1);
                return;
            }

            end = start + new Vector2(-1f, -1f);

            if (bmanager.wallsPositions.Contains(end))
                hit = true;
            else
                hit = false;

            if (!hit)
            {
                ChangeDirection(3);
                return;
            }

            if (distx > 0)
            {
                ChangeDirection(1);
            }
            else if (distx < 0)
            {
                ChangeDirection(3);
            }
            else if (distx == 0)
            {
                if (Random.value > 0.5)
                    ChangeDirection(1);
                else
                    ChangeDirection(3);
            }
            return;
        }

        // LEWO ZABLOKOWANE
        if (dir == direction.left)
        {
            bool hit;

            Vector2 start = transform.position;
            Vector2 end = start + new Vector2(-1f, 1f);

            if (bmanager.wallsPositions.Contains(end))
                hit = true;
            else
                hit = false;

            if (!hit)
            {
                ChangeDirection(0);
                return;
            }

            end = start + new Vector2(-1f, -1f);

            if (bmanager.wallsPositions.Contains(end))
                hit = true;
            else
                hit = false;

            if (!hit)
            {
                ChangeDirection(2);
                return;
            }

            if (disty > 0)
            {
                ChangeDirection(0);
            }
            else if (disty < 0)
            {
                ChangeDirection(2);
            }
            else if (disty == 0)
            {
                if (Random.value > 0.5)
                    ChangeDirection(0);
                else
                    ChangeDirection(2);
            }
            return;
        }

        #endregion
    }


    #endregion

    // Czas do następnego ruchu gracza
    public void TimeToMove()
    {
        if (this == null)
            return;

        if (stunned > 0)
        {
            stunned--;
            return;
        }

        if (!playerInRange)
            return;

        if (!moveHasEnded && type == enemyType.underground)
            return;

        if (enemyStats.curSkillCooldown > 0)
        {
            enemyStats.curSkillCooldown--;
        }

        if (skippedTurns > 0)
        {
            skippedTurns--;
            return;
        }

        skippedTurns = turnSkip;

        if (playerPos != null)
        {
            distx = (int)(playerPos.transform.position.x - transform.position.x);
            disty = (int)(playerPos.transform.position.y - transform.position.y);
        }
        else
            return;

        switch (type)
        {
            case (enemyType.melee):

                #region melee

                Pathfinding(distx, disty);

                AttemptMove<Player>(dirx, diry);

                #endregion

                break;

            case (enemyType.ranged):

                #region ranged

                if (enemyStats.curSkillCooldown <= 0)
                {
                    enemyStats.curSkillCooldown = enemyStats.maxSkillCooldown;

                    if (distx < attackRange && distx > -attackRange)
                    {
                        if (disty > 0)
                            ChangeDirection(0);
                        else
                            ChangeDirection(2);

                        anim.SetTrigger("Attack1");
                        break;
                    }
                    else if (disty < attackRange && disty > -attackRange)
                    {
                        if (distx > 0)
                            ChangeDirection(1);
                        else
                            ChangeDirection(3);

                        int direction = (int)transform.position.x + dirx;

                        if (direction > transform.position.x)
                            sprite.transform.localScale = new Vector2(1f, 1f);
                        else if (direction < transform.position.x)
                            sprite.transform.localScale = new Vector2(-1f, 1f);

                        anim.SetTrigger("Attack1");
                        break;
                    }
                }

                Pathfinding(distx, disty);

                if (Mathf.Abs(distx) < runawayRange && Mathf.Abs(disty) < runawayRange)
                {
                    dirx *= -1;
                    diry *= -1;
                }

                AttemptMove<Player>(dirx, diry);

                #endregion

                break;

            case (enemyType.mage):

                #region mage

                Pathfinding(distx, disty);

                if (enemyStats.curSkillCooldown <= 0)
                {
                    enemyStats.curSkillCooldown = enemyStats.maxSkillCooldown;

                    if (distx < attackRange && distx > -attackRange)
                    {
                        if (disty > 0)
                            ChangeDirection(0);
                        else
                            ChangeDirection(2);

                        anim.SetTrigger("Attack1");
                        break;
                    }
                    else if (disty < attackRange && disty > -attackRange)
                    {
                        if (distx > 0)
                            ChangeDirection(1);
                        else
                            ChangeDirection(3);

                        int direction = (int)transform.position.x + dirx;

                        if (direction > transform.position.x)
                            sprite.transform.localScale = new Vector2(1f, 1f);
                        else if (direction < transform.position.x)
                            sprite.transform.localScale = new Vector2(-1f, 1f);

                        anim.SetTrigger("Attack1");
                        break;
                    }
                }

                if (Mathf.Abs(distx) < runawayRange && Mathf.Abs(disty) < runawayRange)
                {
                    if (dir == direction.up)
                        ChangeDirection(2);
                    else if (dir == direction.down)
                        ChangeDirection(0);

                    if (dir == direction.right)
                        ChangeDirection(3);
                    else if (dir == direction.left)
                        ChangeDirection(1);
                }

                AttemptMove<Player>(dirx, diry);

                #endregion

                break;

            case (enemyType.underground):

                #region underground

                moveHasEnded = false;

                RaycastHit2D hit;

                Vector3 start = transform.position;
                Vector3 end = start + new Vector3(dirx, diry);

                bodyCollider.enabled = false;
                hit = Physics2D.Linecast(start, end, blockingLayer);
                bodyCollider.enabled = true;

                if (hit && hit.collider.gameObject.layer == 9)
                {
                    AttemptMove<Player>(dirx, diry);
                    Invoke("EndMove", 0.2f);
                    return;
                }

                Pathfinding(distx, disty);

                StartCoroutine(moveUnderground(dirx, diry));

                #endregion

                break;
        }
    }

    private void EndMove()
    {
        StartCoroutine(MoveEnded());
    }

    private IEnumerator moveUnderground(int X, int Y)
    {
        int i = 0;
        int dist = 0;

        RaycastHit2D hit;

        Vector3 start;
        Vector3 end;

        if (!burrow)
            BurrowUnburrow();

        if (Mathf.Abs(X) > Mathf.Abs(Y))
        {
            dist = Mathf.Abs(distx);
        }
        else if (Mathf.Abs(Y) > Mathf.Abs(X))
        {
            dist = Mathf.Abs(disty);
        }

        while (i < dist)
        {
            AttemptMove<Player>(dirx, diry);
            i++;

            start = transform.position;
            end = start + new Vector3(dirx, diry);

            bodyCollider.enabled = false;
            hit = Physics2D.Linecast(start, end, blockingLayer);
            bodyCollider.enabled = true;

            if (hit && hit.collider.gameObject.layer != 14)
                break;

            yield return new WaitForSeconds(moveTime);
        }

        StartCoroutine(MoveEnded());

        start = transform.position;
        end = start + new Vector3(dirx, diry);

        bodyCollider.enabled = false;
        hit = Physics2D.Linecast(start, end, blockingLayer);
        bodyCollider.enabled = true;

        if (hit && hit.collider.gameObject.layer == 9)
        {
            BurrowUnburrow();
        }
    }

    // Jeśli przeciwnik nie może się ruszać
    protected override void OnCantMove<T>(T component, Vector3 end)
    {
        player = component as Player;
        if (anim != null)
            anim.SetTrigger("Attack1");
    }

    #region Ataki: DamagePlayer() - zwykły atak melee, Shoot() - strzelanie pociskiem

    public void DamagePlayer()
    {
        RaycastHit2D hit;

        Vector3 start = transform.position;
        Vector3 end = start + new Vector3(dirx, diry);

        bodyCollider.enabled = false;
        hit = Physics2D.Linecast(start, end, blockingLayer);
        bodyCollider.enabled = true;

        if (hit && hit.collider.gameObject.layer == 9)
            player.DamagePlayer(enemyStats.damage, false);
        else
            return;
    }

    public void Shoot()
    {
        GameObject clone = (GameObject)Instantiate(projectile, shootingPoint.position, transform.rotation);

        Rigidbody2D clonesBody = clone.GetComponent<Rigidbody2D>();

        Vector2 throwingForce = new Vector2(dirx * shootingForce, diry * shootingForce);

        clonesBody.AddForce(throwingForce, forceMode);

        clone.transform.Rotate(0, Yrotation, 0);

        if (type == enemyType.mage)
        {
            if (dirx > 0)
            {
                clone.transform.Rotate(0, 0, 0);
            }
            else if (dirx < 0)
            {
                clone.transform.Rotate(-180, 0, 0);
            }
            else if (diry > 0)
            {
                clone.transform.Rotate(90, 0, 0);
            }
            else if (diry < 0)
            {
                clone.transform.Rotate(-90, 0, 0);
            }
        }
    }

    #endregion

    // Zadanie obrażeń przeciwnikowi
    public void DamageEnemy(float dmg)
    {
        if (type == enemyType.underground && burrow)
            return; 

        enemyStats.curHealth -= (int) dmg;
        if (anim != null)
            anim.SetTrigger("Hit");
    }

    // Kiedy enemy is kill
    public void KillEnemy()
    {
        if (Random.value >= 0.66f)
        {
            Instantiate(drops[0], transform.position, Quaternion.identity);
        }

        spellController.curCooldown--;
        itemController.curCooldown--; //Kamil - 17.02.2016 - odejmowanie cooldownu od itemu po zabiciu przeciwnika

        player.playerStats.experience += experience[(int)type];

        //Zwiększenie licznika zabitych przeciwników dla przedmiotów Łowca Dusz, jeśli jest aktywny
        if (itemController.type == ItemController.Item.soulHunter) itemController.enemiesKilledForSoulHunter++;
        if (itemController.type == ItemController.Item.necromancerMask && itemController.isActive)
        {
            if (player.playerStats.curHealth + player.playerStats.magicDamage >= player.playerStats.maxHealth) player.playerStats.curHealth = player.playerStats.maxHealth;
            else player.playerStats.curHealth += player.playerStats.magicDamage;            
        }

        if (anim != null)
            anim.SetTrigger("Death");

        Destroy(gameObject, 0f);
        Invoke("Clock.turns.ListEnemies", 0.001f);
        GameObject particles = (GameObject)Instantiate(deathParticles, transform.position, transform.rotation);
        Destroy(particles, 1f);
    }

    void OnTriggerEnter2D(Collider2D other) //Kamil - potrzebne do tworzenia bomby
    {
        if (other.gameObject.name.Contains("BombToSpawn"))
        {
            DamageEnemy(player.playerStats.magicDamage * 3.1f);
        }

        if (other.gameObject.tag == "Lance")
        {
            DamageEnemy(player.playerStats.physicalDamage * 5f);
        }

        if (other.gameObject.tag == "Player" && spellController.type == SpellController.Spell.sacredAttack && spellController.isActive)
        {
            DamageEnemy(player.playerStats.magicDamage * 4f);
        }

        if (other.gameObject.name.Contains("PoisonTile"))
        {
            DamageEnemy(GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().playerStats.magicDamage);
        }

        if (other.gameObject.layer == 15)
        {
            playerInRange = true;
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.layer == 15)
        {
            playerInRange = true;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.layer == 15)
        {
            playerInRange = false;
        }
    }
}