﻿using UnityEngine;
using System.Collections;

public abstract class MoveObject : MonoBehaviour
{

    [Header("Move")]
    public LayerMask blockingLayer;
    public float moveTime;

    protected Collider2D bodyCollider;
    protected Rigidbody2D body;
    [HideInInspector]
    public Transform sprite;
    [HideInInspector]
    public Animator anim;

    protected bool moveHasEnded;

    protected virtual void Start()
    {
        moveHasEnded = true;
        anim = GetComponent<Animator>();
        bodyCollider = GetComponent<Collider2D>();
        body = GetComponent<Rigidbody2D>();
        sprite = transform.Find("Sprite");
    }

    protected bool Move(int xDir, int yDir, out RaycastHit2D hit)
    {
        // Początkowa pozycja obiektu
        Vector3 start = transform.position;
        // Docelowa pozycja obiektu
        Vector3 end = start + new Vector3(xDir, yDir);

        // Wyłączenie collidera, żeby linecast nie wykrył samego siebie (czy coś takiego kek)
        bodyCollider.enabled = false;
        hit = Physics2D.Linecast(start, end, blockingLayer);
        bodyCollider.enabled = true;

        // Jeśli niczego nie wykryło wykonaj ruch
        if (hit.transform == null)
        {
            StartCoroutine(SmoothMove(start, end));
            return true;
        }

        return false;
    }

    protected virtual void AttemptMove<T>(int xDir, int yDir) where T : Component
    {
        RaycastHit2D hit;

        // Do obracania sprite'a
        Vector3 direction = transform.position + new Vector3(xDir, yDir);

        // Obracanie sprite'a jeśli porusza się w lewo
        if (direction.x > transform.position.x)
            sprite.transform.localScale = new Vector2(1f, 1f);
        else if (direction.x < transform.position.x)
            sprite.transform.localScale = new Vector2(-1f, 1f);

        // Może się poruszyć
        bool canMove = Move(xDir, yDir, out hit);

        if (hit.transform == null)
            return;

        // hitComponent zwraca obiekt z którym doszło do kolizji
        T hitComponent = hit.transform.GetComponent<T>();

        if (!canMove && hitComponent != null)
        {
            // Wywołuje funkcje OnCantMove
            OnCantMove(hitComponent, direction);
        }
    }

    protected IEnumerator SmoothMove(Vector3 start, Vector3 end)
    {
        float remainingDist = (transform.position - end).sqrMagnitude;

        // Animacja poruszania sie
        if (anim != null)
            anim.SetTrigger("Walk");

        while (remainingDist > float.Epsilon)
        {
            Vector3 newPos = Vector3.MoveTowards(body.position, end, 10f * Time.deltaTime);
            body.MovePosition(newPos);

            bodyCollider.enabled = false;
            RaycastHit2D hit = Physics2D.Linecast(start, end, blockingLayer);
            bodyCollider.enabled = true;

            if (hit && hit.collider.gameObject.layer == 9)
            {
                transform.position = new Vector2((int)transform.position.x, (int)transform.position.y);
                Player hitComponent = hit.transform.GetComponent<Player>();
                OnCantMove(hitComponent, end);
                break;
            }
            if (hit)
            {                
                break;
            }

            remainingDist = (transform.position - end).sqrMagnitude;
            yield return null;
        }

        Vector2 curPos = new Vector2(transform.position.x, transform.position.y);
        Vector2 nextPos = new Vector2((int)transform.position.x, (int)transform.position.y);

        if (curPos != nextPos)
            transform.position = nextPos;
    }

    // Jeśli doszło do kolizji z czymś wykonaj to:
    protected virtual void OnCantMove<T>(T component, Vector3 end)
            where T : Component
    {
        Vector3 start = transform.position;

        bodyCollider.enabled = false;
        RaycastHit2D hit = Physics2D.Linecast(start, end, blockingLayer);
        bodyCollider.enabled = true;

        if (!hit)
        {
            return;
        }
    }

    protected IEnumerator MoveEnded()
    {
        moveHasEnded = !moveHasEnded;
        yield return null;
    }
}
