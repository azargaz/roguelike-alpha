﻿using UnityEngine;
using System.Collections;

public class ExplosionMagics : MonoBehaviour
{

    public GameObject boomParticles;

    public float lifeTime;
    private float timeLeft;

    void Awake()
    {               
        timeLeft = Time.time + lifeTime;
    }

    void LateUpdate()
    {        
        if (Time.time > timeLeft)
        {
            GameObject clone = (GameObject) Instantiate(boomParticles, transform.position, Quaternion.identity);
            Destroy(clone, 1f);

            Destroy(gameObject, 0f);
        }
    }

    void OnCollisionEnter2D (Collision2D other)
    {
        timeLeft = 0f;
    }
}
