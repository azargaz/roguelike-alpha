﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelUpMenu : MonoBehaviour {

    public Text[] statsText = new Text[6];
    public GameObject[] up = new GameObject[3];
    private Player player;

	void Start ()
    {
        if(player == null)
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }
	
	void Update ()
    {
        if(player == null)
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

        if (player != null)
        {
            statsText[0].text = "Zdrowie: " + player.playerStats.health;
            statsText[1].text = "Siła: " + player.playerStats.strength;
            statsText[2].text = "Magia: " + player.playerStats.magic;
            statsText[3].text = "Punkty zdrowia: " + player.playerStats.curHealth + " / " + player.playerStats.maxHealth;
            statsText[4].text = "Obrażenia fizyczne: " + player.playerStats.physicalDamage;
            statsText[5].text = "Obrażenia magiczne: " + player.playerStats.magicDamage;
            statsText[6].text = "Poziom: " + player.playerStats.level;
            statsText[7].text = "Doświadczenie: " + player.playerStats.experience + " / " + player.playerStats.expToNextLvl;
            statsText[8].text = "Punkty do rozdania: " + player.playerStats.up;
            statsText[9].text = "Ochrona przed magią: " + 1f / (10f / player.playerStats.magicDamage);
        }

        if (player.playerStats.up <= 0)
        {
            for(int i = 0; i < up.Length; i++)
            {
                up[i].SetActive(false);
            }
        }
        else if (player.playerStats.up > 0)
        {
            for (int i = 0; i < up.Length; i++)
            {
                up[i].SetActive(true);
            }
        }
    }

    public void healthUp()
    {
        if(player.playerStats.up > 0)
            player.playerStats.healthUp();
    }

    public void damageUp()
    {
        if (player.playerStats.up > 0)
            player.playerStats.strengthUp();
    }

    public void magicUp()
    {
        if (player.playerStats.up > 0)
            player.playerStats.magicUp();
    }
}
