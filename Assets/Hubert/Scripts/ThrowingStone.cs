﻿using UnityEngine;
using System.Collections;

public class ThrowingStone : MonoBehaviour {

    private Player player;
    private Enemy enemy;

	void Awake ()
    {
        enemy = GameObject.Find("Skeleton_ranged(Clone)").GetComponent<Enemy>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
	}
	
	void OnCollisionEnter2D (Collision2D other)
    {
        if (other.gameObject.layer == 9)
        {
            if (player != null && enemy != null)
                player.DamagePlayer(enemy.enemyStats.damage, false);
            else
            {
                Destroy(gameObject, 0f);
                return;
            }
        }
        Destroy(gameObject, 0f);
    }
}
