﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour
{

    private Player player;
    private Enemy enemy;
    private CircleCollider2D trigger;

    public float dmgTime;
    private float timeLeft;

    private bool playerInRangeOfMassacre;

    void Awake()
    {
        trigger = GetComponent<CircleCollider2D>();
        enemy = GameObject.Find("Skeleton_mage(Clone)").GetComponent<Enemy>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    void Update()
    {        
        trigger.radius += (Time.deltaTime * 2.8f);
        if (Time.time > timeLeft)
        {
            if (playerInRangeOfMassacre)
            {
                timeLeft = Time.time + dmgTime;
                player.DamagePlayer(enemy.enemyStats.damage, true);
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == 9)
        {
            playerInRangeOfMassacre = true;
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.layer == 9)
        {
            playerInRangeOfMassacre = true;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.layer == 9)
        {
            playerInRangeOfMassacre = false;
        }
    }
}
