﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

    [SerializeField] Transform player;
	
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

	void FixedUpdate ()
    {
        if (player == null) player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        else
        {
            Vector2 velocity = Vector2.zero;
            transform.position = Vector2.SmoothDamp(transform.position, player.transform.position, ref velocity, 0.1f);
            transform.position = new Vector3(transform.position.x, transform.position.y, -10f);
        }
	}
}
