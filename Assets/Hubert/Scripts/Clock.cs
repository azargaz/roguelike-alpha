﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Clock : MonoBehaviour
{

    public static Clock turns = null;

    public List<Enemy> enemies;
    private Player player;

    public bool enemiesMoving = false;
    public bool playersTurn = false;
    public bool playerCanAttack = true;
    public int countingPlayerMoves;

    void Awake()
    {
        turns = this;
        playersTurn = false;
        enemies = new List<Enemy>();
    }

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    void Update()
    {
        if (playersTurn || enemiesMoving || GameManager.instance.doingSetup)
            return;

        StartCoroutine(MoveEnemies());
    }

    public void ListEnemies()
    {
        enemies.Clear();
        GameObject[] enemyArray = GameObject.FindGameObjectsWithTag("Enemy");
        for (int i = 0; i < enemyArray.Length; i++)
        {
            enemies.Add(enemyArray[i].GetComponent<Enemy>());
        }
    }

    IEnumerator MoveEnemies()
    {
        enemiesMoving = true;

        playerCanAttack = false;
        yield return new WaitForSeconds(player.moveTime);
        playerCanAttack = true;

        if (enemies.Count == 0)
        {
            Debug.Log("Nie ma przeciwników.");
        }
        else
        {

            for (int i = 0; i < enemies.Count; i++)
            {
                enemies[i].TimeToMove();
            }
        }

        yield return new WaitForSeconds(player.moveTime);

        playersTurn = true;

        countingPlayerMoves++;

        enemiesMoving = false;
    }
}
