﻿using UnityEngine;
using System.Collections;

public class PlayerHPBar : MonoBehaviour {

    private Player player;

    private enum barType { hpBar, shieldBar, itemBar, spellBar, expBar};
    [SerializeField]
    private barType type;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }


	void FixedUpdate ()
    {
        if(player == null)
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

        switch (type)
        {
            case (barType.hpBar):
                float curHP = player.playerStats.curHealth;
                float maxHP = player.playerStats.maxHealth;

                if (curHP < 0)
                    curHP = 0;

                float health = curHP / maxHP;

                if (health > 1)
                    health = 1;

                transform.localScale = new Vector2(health, transform.localScale.y);
                break;

            case (barType.shieldBar):
                float curSh = player.playerStats.curShield;
                float maxSh = player.playerStats.maxShield;

                float shield = curSh / maxSh;

                if (shield <= 0)
                    shield = 0;

                if (shield > 1)
                    shield = 1;

                transform.localScale = new Vector2(shield, transform.localScale.y);
                break;

            case (barType.itemBar):
                int itemCD = ItemController.itemcontroller.curCooldown;
                int maxitemCD = ItemController.itemcontroller.itemCooldowns[(int)ItemController.itemcontroller.type];

                float itemBarWidth = 0f;

                if (maxitemCD == 0f)
                    itemBarWidth = 0f;
                else
                    itemBarWidth = (float) (maxitemCD - itemCD) / maxitemCD;

                if (itemBarWidth <= 0)
                    itemBarWidth = 0;
                if (itemBarWidth > 1)
                    itemBarWidth = 1;

                transform.localScale = new Vector2(itemBarWidth, transform.localScale.y);
                break;

            case (barType.spellBar):
                int spellCD = SpellController.spellController.curCooldown;
                int maxspellCD = SpellController.spellController.spellCooldown[(int)SpellController.spellController.type];

                float spellBarWidth = 0f;

                if (maxspellCD == 0f)
                    spellBarWidth = 0f;
                else
                    spellBarWidth = (float) (maxspellCD - spellCD) / maxspellCD;

                if (spellBarWidth <= 0)
                    spellBarWidth = 0;
                if (spellBarWidth > 1)
                    spellBarWidth = 1;

                transform.localScale = new Vector2(spellBarWidth, transform.localScale.y);
                break;

            case (barType.expBar):
                int curExp = player.playerStats.experience;
                int maxExp = player.playerStats.expToNextLvl;

                float exp = (float) curExp / maxExp;

                if (exp <= 0)
                    exp = 0;

                if (exp > 1)
                    exp = 1;

                transform.localScale = new Vector2(exp, transform.localScale.y);
                break;
        }
	}
}
