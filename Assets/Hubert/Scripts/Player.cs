﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Player : MoveObject
{

    [System.Serializable]
    public class PlayerStats
    {
        // Level i LvlUp
        public int level = 1;
        public int experience = 0;
        public int expToNextLvl;
        public int up = 0;

        public void LevelUp()
        {
            level++;
            up++;
            refreshStats();
        }

        // Bazowe staty
        public float baseHealth = 10;
        public float baseStrength = 10;
        public float baseMagic = 10;

        // Aktualne staty
        public float health = 10;
        public float strength = 10;
        public float magic = 10;

        // Bonus do hp/ataku/magii
        public float healthMultiplier;
        public float PhysicalDmgMultiplier;
        public float MagicDmgMultiplier;

        // Podnoszenie statów
        public void healthUp()
        {
            if (up > 0)
            {
                up--;
                health += 1;
                if (health <= 20)
                {
                    healthMultiplier += 10;
                    curHealth += 10;
                }
                else if (health > 20 && health <= 30)
                {
                    healthMultiplier += 5;
                    curHealth += 5;
                }
                else
                {
                    healthMultiplier += 1;
                    curHealth += 1;
                }

                refreshStats();
            }
            else
                return;
        }    
        public void strengthUp()
        {
            if (up > 0)
            {
                up--;
                strength += 1;
                if(strength <= 15)
                {
                    PhysicalDmgMultiplier += 1;
                }
                else if (strength > 15)
                {
                    PhysicalDmgMultiplier += 0.5f;
                }
                refreshStats();
            }
            else
                return;
        }
        public void magicUp()
        {
            if (up > 0)
            {
                up--;
                magic += 1;
                if (magic <= 20)
                {
                    MagicDmgMultiplier += 1;
                }
                else if (magic > 20)
                {
                    MagicDmgMultiplier += 0.5f;
                }
                refreshStats();
            }
            else
                return;
        }

        // Zdrowie i tarcza
        public float maxHealth = 10;
        public float curHealth = 0;

        public float maxShield = 10;
        public float curShield = 0;

        // Obrażenia     
        public float physicalDamage;
        public float magicDamage;
        
        // Restart statów
        public void resetStats()
        {
            health = baseHealth;
            strength = baseStrength;
            magic = baseMagic;

            healthMultiplier = 0;
            PhysicalDmgMultiplier = 0;
            MagicDmgMultiplier = 0;            

            maxHealth = 100 + healthMultiplier;
            curHealth = maxHealth;
            curShield = 0;

            onFire = 0;       
            up = 0;
            experience = 0;
            level = 1;        
        }

        // Odświerzanie statów
        public void refreshStats()
        {
            expToNextLvl = (int) ( 5 * Mathf.Pow(level, 2) - 15 * level + 25 );

            maxHealth = 100 + healthMultiplier;
            physicalDamage = 10 + PhysicalDmgMultiplier;
            magicDamage = 10 + MagicDmgMultiplier;        
        }

        // Zapobieganie większemu niż max hp i tarczy
        public void maximumHP()
        {
            if (curHealth > maxHealth)
                curHealth = maxHealth;
            if (curShield > maxShield)
                curShield = maxShield;
            if (curShield < 0)
                curShield = 0;
        }

        public float attackCooldown;

        public bool dead = false;

        public int onFire = 0;

        public bool godMode;

        public void GodMode()
        {
            curHealth = 1f;
        }
    }

    [Header("Stats")]
    public PlayerStats playerStats = new PlayerStats();

    [HideInInspector]
    public int horizontal = 0;
    [HideInInspector]
    public int vertical = 0;

    [Header("Other")]
    public GameObject deathParticles;

    public GameObject handsPos;
    public GameObject feetPos;
    public GameObject masochismParticles;
    public GameObject midasHandParticles;
    public GameObject poisonParticles;
    public GameObject fireParticles;

    private Enemy enemy;
    private ChestController chest;
    public ItemController itemController;
    public BoardManager boardManager;
    public TrapsController trapsController;
    public SpellController spellController;
    public GameObject HUD;

    [SerializeField]
    LayerMask floorMask; //Kamil - LayerMask potrzebny do raycastu; Potrzebne do maski Midasa

    void Awake()
    {        
        playerStats.resetStats();
        playerStats.refreshStats();

        if (HUD == null) HUD = GameObject.FindGameObjectWithTag("HUD");
        if (itemController == null) itemController = GameObject.Find("GameManager").GetComponent<ItemController>();
        if (boardManager == null) boardManager = GameObject.Find("GameManager").GetComponent<BoardManager>();
        if (trapsController == null) trapsController = GameObject.Find("GameManager").GetComponent<TrapsController>();
        if (spellController == null) spellController = GameObject.Find("GameManager").GetComponent<SpellController>();
    }

    protected override void Start()
    {
        // funkcja Start z skryptu MoveObject
        base.Start();
    }

    void Update()
    {
        // Levelowanie
        if(playerStats.experience >= playerStats.expToNextLvl)
        {
            if(playerStats.experience == playerStats.expToNextLvl)
            {
                playerStats.experience = 0;
            }
            else if (playerStats.experience > playerStats.expToNextLvl)
            {
                playerStats.experience = playerStats.experience - playerStats.expToNextLvl;
            }
            playerStats.LevelUp();
        }

        // Podpalenie
        if(playerStats.onFire > 0)
        {
            fireParticles.SetActive(true);
        }
        else if(playerStats.onFire <= 0)
        {
            fireParticles.SetActive(false);
        }

        // Efekt niewidzialności
        if (spellController.stealthBool)
        {
            sprite.GetComponent<SpriteRenderer>().color = new Vector4(1f, 1f, 1f, 0.5f);
        }
        else if (!spellController.stealthBool)
        {
            sprite.GetComponent<SpriteRenderer>().color = new Vector4(1f, 1f, 1f, 1f);
        }

        // Godmode
        if (playerStats.godMode)
        {
            playerStats.GodMode();
        }

        if (spellController.type == SpellController.Spell.teleport && spellController.isActive && spellController.curCooldown == 0)
        {
            //Teleportacja. Wymagane sprawdzanie tego warunku, ponieważ w innym wypadku skrypt zaklęcia wpada w pętle
            //Nie wiem czemu, dodajmy to po prostu do zagadek ludzkości. Działa? Działa, więc nie ruszać mi tego.
        }

        else if (spellController.type == SpellController.Spell.backstab && spellController.isActive && spellController.curCooldown == 0)
        {
            //To samo co na górze, ale potrzebne do backstaba
        }

        // Wczytanie przycisków poruszania się
        else if (itemController.type == ItemController.Item.cursedTalisman)
        {
            horizontal = vertical = 0;

            if (Input.GetKey(KeyCode.DownArrow)) itemController.GettingRandomMovement_CursedTalisman(itemController.downArrow);
            else if (Input.GetKey(KeyCode.UpArrow)) itemController.GettingRandomMovement_CursedTalisman(itemController.upArrow);
            else if (Input.GetKey(KeyCode.RightArrow)) itemController.GettingRandomMovement_CursedTalisman(itemController.rightArrow);
            else if (Input.GetKey(KeyCode.LeftArrow)) itemController.GettingRandomMovement_CursedTalisman(itemController.leftArrow);
        }
        else
        {
            horizontal = (int)(Input.GetAxisRaw("Horizontal"));
            vertical = (int)(Input.GetAxisRaw("Vertical"));
        }

        // Usuń to, żeby odblkować chodzenie na skos
        if (horizontal != 0)
        {
            vertical = 0;
        }

        // Poruszanie się

        if ((horizontal != 0 || vertical != 0) && Clock.turns.playersTurn && !GameManager.instance.doingSetup)
            if (itemController.type != ItemController.Item.lance || (itemController.type == ItemController.Item.lance && !itemController.isActive)) //Kamil - gracz powinien zostać w miejscu gdy używa lancy
            {
                TimeToMove(horizontal, vertical);
                Clock.turns.playersTurn = false;
            }

        playerStats.maximumHP();

        if (playerStats.curHealth <= 0 && !playerStats.godMode && !playerStats.dead)
        {
            if (spellController.type == SpellController.Spell.revival && spellController.isActive)
                return;
            anim.SetTrigger("Death");
            playerStats.dead = true;
        }    
    }

    protected override void AttemptMove<T>(int xDir, int yDir)
    {
        // funkcja AttemptMove z skryptu MoveObject
        base.AttemptMove<T>(xDir, yDir);
    }

    // Czas do następnego ruchu gracza
    public void TimeToMove(int horizontal, int vertical)
    {
        if (playerStats.onFire > 0)
        {
            playerStats.onFire--;
            if(playerStats.onFire % 4 == 0)
            {
                playerStats.curHealth -= 10;
                anim.SetTrigger("Hit");
            }
        }

        AttemptMove<Enemy>(horizontal, vertical);
        AttemptMove<ChestController>(horizontal, vertical);
    }

    // Jeśli gracz nie może się ruszać
    protected override void OnCantMove<T>(T component, Vector3 end)
    {
        enemy = component as Enemy;

        chest = component as ChestController;

        if (enemy == null && chest == null)
            return;

        if (chest != null)
        {
            StartCoroutine(chest.OpenChest());
            return;
        }
        else if (enemy != null)
        {
            if (Clock.turns.playerCanAttack && Time.time >= playerStats.attackCooldown)
            {
                playerStats.attackCooldown = moveTime + Time.time;
                anim.SetTrigger("Attack1");
            }
        }
    }


    // Zadanie obrażeń graczowi
    public void DamagePlayer(float dmg, bool magic)
    {
        if (itemController.type == ItemController.Item.shield && itemController.isActive) //Kamil - używanie tarczy
        {
            itemController.isActive = false; //Zużycie tarczy po bloku gracza
            itemController.IncreasingCDAfterShield();
        }

        else if (spellController.type == SpellController.Spell.masochism && spellController.isActive)
        {
            playerStats.curShield++;
        }

        else
        {
            if (!magic)
            {
                if (playerStats.curShield > 0)
                    playerStats.curShield -= 1;
                else
                    playerStats.curHealth -= (int) dmg;
            }
            else if (magic)
            {
                playerStats.curHealth -= (int) (dmg * (10f / playerStats.magicDamage));
            }
            anim.SetTrigger("Hit");
        }
    }
    // Do animacji ataku
    public void DamageEnemy()
    {
        if (itemController.type == ItemController.Item.midasHand && itemController.isActive) //Kamil - używanie ręki midasa
        {
            //Ustawianie statystyk przeciwnika tak, aby podczas ataku gracza był na jedno uderzenie i nie mógł w tym samym czasie co gracz zadać mu obrażań
            enemy.GetComponent<Enemy>().enemyStats.maxHealth = 1;
            enemy.GetComponent<Enemy>().enemyStats.curHealth = 1;
            enemy.GetComponent<Enemy>().enemyStats.damage = 0;
            enemy.GetComponent<Enemy>().turnSkip = 100;
            //Nieudana próba zmiany koloru. Prawdopodbnie wynika to z tego, że przeciwnik jest zielony
            enemy.GetComponentInChildren<SpriteRenderer>().color = new Color(1f, 1f, 0f, 1f);
        }

        enemy.DamageEnemy(playerStats.physicalDamage);
    }

    // Kiedy player is kill
    public void KillPlayer()
    {
        if (itemController.extraLife)
        {
            playerStats.curHealth = 75; //Jeśli gracz zebrać dodatkowe życie jest wkrzeszany (bez animacji, po prostu odzyskuje zdrowie do 75hp)
            itemController.extraLife = false; //Wyłączenie po zużyciu
        }
        else
        {            
            GameManager.instance.InvokeGameOver();            
        }
    }

    public void DeathParticles()
    {
        GameObject particles = (GameObject)Instantiate(deathParticles, transform.position, transform.rotation);
        Destroy(particles, 2f);
    }

    public void DisableSprite()
    {
        sprite.gameObject.SetActive(false);        
    }

    /*****************************************************/
    List<Vector3> corPlayerPos = new List<Vector3>();
    bool enteredCorridor;
    List<GameObject> corridors = new List<GameObject>();

    void FixedUpdate()
    {
        if (enteredCorridor)
        {
            if (corPlayerPos[0].y == corPlayerPos[1].y) //Jeśli korytarz jest poziomy
            {
                //Sprawdzanie czy gracz wyszedł z korytarza
                if (corPlayerPos[0] == (new Vector3(gameObject.transform.position.x - 1, gameObject.transform.position.y, 0f))
                     || (corPlayerPos[1] == (new Vector3(gameObject.transform.position.x + 1, gameObject.transform.position.y, 0f))))
                {
                    for (int i = 0; i < corridors.Count; i++)
                    {
                        corridors[i].GetComponent<BoxCollider2D>().isTrigger = true; //Włączanie triggerów
                    }
                    enteredCorridor = false; //Upewnienie się, że warunek nie będzie więcej sprawdzany
                    corridors.Clear(); //Czyszczenie gameObjectów
                }
            }

            else if (corPlayerPos[0].x == corPlayerPos[1].x) //Jeśli korytarz jest pionowy
            {
                //Sprawdzanie czy gracz wyszedł z korytarza
                if (corPlayerPos[0] == (new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - 1, 0f))
                    || (corPlayerPos[1] == (new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 1, 0f))))
                {
                    for (int i = 0; i < corridors.Count; i++)
                    {
                        corridors[i].GetComponent<BoxCollider2D>().isTrigger = true; //Włączanie triggerów
                    }
                    enteredCorridor = false; //Upewnienie się, że warunek nie będzie więcej sprawdzany
                    corridors.Clear(); //Czyszczenie gameObjectów
                }
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "EnemyDrops")
        {
            Destroy(other.gameObject);
            playerStats.curHealth += 10;
        }

        if (other.gameObject.name.Contains("Corridor") && other.gameObject.tag != "Trap")
        {
            if (itemController == null) itemController = GameObject.Find("GameManager").GetComponent<ItemController>();
            if (spellController == null) spellController = GameObject.Find("GameManager").GetComponent<SpellController>();


            if (itemController.isActive == true)
                itemController.isActive = false;

            if (spellController.isActive == true)
                spellController.isActive = false;

            if (spellController.isActive == false || itemController.isActive == false)
            {
                corPlayerPos.Clear(); //Czyszczenie pozycji przed rozpoczęciem przydzielania nowych
                corridors.Clear(); //Czyszczenie gameObjectów korytarzy
                GameObject corridor = other.gameObject.transform.parent.gameObject; //Pobieranie parenta dla corriorTiles;

                List<GameObject> childsList = new List<GameObject>(); //Tworzenie listy

                foreach (Transform child in corridor.transform)
                {
                    childsList.Add(child.gameObject); //Dodanie do listy dziecka korytarza
                    child.GetComponent<BoxCollider2D>().isTrigger = false; //Wyłączanie triggerów
                    corridors.Add(child.gameObject); //Dodawanie gameObjectów do listy
                }

                for (int i = 0; i < childsList.Count; i++)
                {
                    if (i == 0 || i == childsList.Count - 1) //Pobieranie dwóch skrajnych podłóg w korytarzu
                    {
                        corPlayerPos.Add(childsList[i].transform.position); //Dodawanie ich do listy
                    }
                }

                if (corPlayerPos[0].x <= corPlayerPos[1].x && corPlayerPos[0].y <= corPlayerPos[1].y) //Sortowanie
                {
                    Vector3 holder = corPlayerPos[0];
                    corPlayerPos[0] = corPlayerPos[1];
                    corPlayerPos[1] = holder;
                }

                enteredCorridor = true; //Bool, który pozwala wejść do FixedUpdate
            }
        }
        if (other.gameObject.tag == "Finish")
        {
            StopAllCoroutines();
            boardManager.ClearingMap(other);            
        }

        if (other.gameObject.tag == "Trap")
        {
            if (trapsController == null) trapsController = GameObject.Find("GameManager").GetComponent<TrapsController>();
            if (other.gameObject.name == "Spikes(Clone)")
            {
                trapsController.ActivatingSpikes(other);
            }

            if (other.gameObject.name.Contains("Curse"))
            {
                trapsController.Curse();
                other.GetComponent<SpriteRenderer>().enabled = true;
            }

            if (other.gameObject.name.Contains("CorridorActivator"))
            {
                other.gameObject.GetComponent<CorridorSpikesActivator>().Activation();
            }

            if (other.gameObject.name.Contains("RoomSpikesActivator"))
            {
                other.gameObject.GetComponent<RoomSpikesActivationController>().Activation();
            }

            if (other.gameObject.name.Contains("Fire"))
            {
                other.gameObject.GetComponent<FireController>().ActivatingFire(other);
            }
        }
    }
}