﻿using UnityEngine;
using System.Collections;

public class RoomSpikesActivationController : MonoBehaviour
{
    BoardManager boardManager;
    [SerializeField] GameObject spikes;

    void Start()
    {
        boardManager = GameObject.Find("GameManager").GetComponent<BoardManager>();
    }

    public void Activation()
    {
        /************* ZETKNIĘCIE Z ROGAMI **************/
        if (boardManager.wallsPositions.Contains(new Vector3(gameObject.transform.position.x - 1, gameObject.transform.position.y, 0f))
            && boardManager.wallsPositions.Contains(new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - 1, 0f))) //Lewy dolny róg
        {
            Instantiate(spikes, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 2, 0f), gameObject.transform.rotation);
            Instantiate(spikes, new Vector3(gameObject.transform.position.x + 2, gameObject.transform.position.y, 0f), gameObject.transform.rotation);
        }

        else if (boardManager.wallsPositions.Contains(new Vector3(gameObject.transform.position.x - 1, gameObject.transform.position.y, 0f))
            && boardManager.wallsPositions.Contains(new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 1, 0f))) //Lewy górny róg
        {
            Instantiate(spikes, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - 2, 0f), gameObject.transform.rotation);
            Instantiate(spikes, new Vector3(gameObject.transform.position.x + 2, gameObject.transform.position.y, 0f), gameObject.transform.rotation);
        }

        else if (boardManager.wallsPositions.Contains(new Vector3(gameObject.transform.position.x + 1, gameObject.transform.position.y, 0f))
            && boardManager.wallsPositions.Contains(new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 1, 0f))) //Prawy górny róg
        {
            Instantiate(spikes, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - 2, 0f), gameObject.transform.rotation);
            Instantiate(spikes, new Vector3(gameObject.transform.position.x - 2, gameObject.transform.position.y, 0f), gameObject.transform.rotation);
        }

        else if (boardManager.wallsPositions.Contains(new Vector3(gameObject.transform.position.x + 1, gameObject.transform.position.y, 0f))
            && boardManager.wallsPositions.Contains(new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - 1, 0f))) //Prawy dolny róg
        {
            Instantiate(spikes, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 2, 0f), gameObject.transform.rotation);
            Instantiate(spikes, new Vector3(gameObject.transform.position.x - 2, gameObject.transform.position.y, 0f), gameObject.transform.rotation);
        }
        /*********************************************/
        /********** KONTAKTY Z PROSTYMI ŚCIANAMI *****/
        else if (boardManager.wallsPositions.Contains(new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - 1, 0f)) //Zetknięcie z poziomą ścianą
                     || boardManager.wallsPositions.Contains(new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 1, 0f)))
        {
            //Jeśli po prawej stronie jest przejście:
            if (!boardManager.wallsPositions.Contains(new Vector3(gameObject.transform.position.x + 2, gameObject.transform.position.y - 1, 0f))
                || !boardManager.wallsPositions.Contains(new Vector3(gameObject.transform.position.x + 2, gameObject.transform.position.y + 1, 0f)))
            {
                if (boardManager.corridorPositions.Contains(new Vector3(gameObject.transform.position.x + 2, gameObject.transform.position.y - 1, 0f))
                || boardManager.corridorPositions.Contains(new Vector3(gameObject.transform.position.x + 2, gameObject.transform.position.y + 1, 0f)))
                {
                    Instantiate(spikes, new Vector3(gameObject.transform.position.x - 2, gameObject.transform.position.y, 0f), gameObject.transform.rotation);
                    Instantiate(spikes, new Vector3(gameObject.transform.position.x + 3, gameObject.transform.position.y, 0f), gameObject.transform.rotation);
                }
            }
            //Jeśli po lewej stronie jest przejście
            else if (!boardManager.wallsPositions.Contains(new Vector3(gameObject.transform.position.x - 2, gameObject.transform.position.y - 1, 0f))
                || !boardManager.wallsPositions.Contains(new Vector3(gameObject.transform.position.x - 2, gameObject.transform.position.y + 1, 0f)))
            {
                if (boardManager.corridorPositions.Contains(new Vector3(gameObject.transform.position.x - 2, gameObject.transform.position.y - 1, 0f))
                || boardManager.corridorPositions.Contains(new Vector3(gameObject.transform.position.x - 2, gameObject.transform.position.y + 1, 0f)))
                {
                    Instantiate(spikes, new Vector3(gameObject.transform.position.x + 2, gameObject.transform.position.y, 0f), gameObject.transform.rotation);
                    Instantiate(spikes, new Vector3(gameObject.transform.position.x - 3, gameObject.transform.position.y, 0f), gameObject.transform.rotation);
                }
            }
            //Normalny spawn:
            else
            {
                Instantiate(spikes, new Vector3(gameObject.transform.position.x - 2, gameObject.transform.position.y, 0f), gameObject.transform.rotation);
                Instantiate(spikes, new Vector3(gameObject.transform.position.x + 2, gameObject.transform.position.y, 0f), gameObject.transform.rotation);
            }
            
        }

        else if (boardManager.wallsPositions.Contains(new Vector3(gameObject.transform.position.x - 1, gameObject.transform.position.y, 0f)) //Zetknięcie z pionową ścianą
                     || boardManager.wallsPositions.Contains(new Vector3(gameObject.transform.position.x + 1, gameObject.transform.position.y, 0f)))
        {
            if ((!boardManager.wallsPositions.Contains(new Vector3(gameObject.transform.position.x + 1, gameObject.transform.position.y + 2, 0f))
                || !boardManager.wallsPositions.Contains(new Vector3(gameObject.transform.position.x - 1, gameObject.transform.position.y + 2, 0f))))
            {
                //Jeśli na górze jest przejście:
                if (!boardManager.wallsPositions.Contains(new Vector3(gameObject.transform.position.x + 1, gameObject.transform.position.y + 2, 0f))
                    || !boardManager.wallsPositions.Contains(new Vector3(gameObject.transform.position.x - 1, gameObject.transform.position.y + 2, 0f)))
                {
                    if (boardManager.corridorPositions.Contains(new Vector3(gameObject.transform.position.x + 1, gameObject.transform.position.y + 2, 0f)) ||
                        boardManager.corridorPositions.Contains(new Vector3(gameObject.transform.position.x - 1, gameObject.transform.position.y + 2, 0f)))
                    {
                        Debug.Log("AAA");
                        Instantiate(spikes, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 3, 0f), gameObject.transform.rotation);
                        Instantiate(spikes, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - 2, 0f), gameObject.transform.rotation);
                    }
                }
                //Jeśli na dole jest przejście
                else if (!boardManager.wallsPositions.Contains(new Vector3(gameObject.transform.position.x - 1, gameObject.transform.position.y - 2, 0f))
                    || !boardManager.wallsPositions.Contains(new Vector3(gameObject.transform.position.x + 1, gameObject.transform.position.y - 2, 0f)))
                {
                    if (boardManager.corridorPositions.Contains(new Vector3(gameObject.transform.position.x - 1, gameObject.transform.position.y - 2, 0f))
                    || boardManager.corridorPositions.Contains(new Vector3(gameObject.transform.position.x + 1, gameObject.transform.position.y - 2, 0f)))
                    {
                        Debug.Log("BBB");
                        Instantiate(spikes, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 2, 0f), gameObject.transform.rotation);
                        Instantiate(spikes, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - 3, 0f), gameObject.transform.rotation);
                    }
                }
                //Normalny spawn:
                else
                {
                    Instantiate(spikes, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 2, 0f), gameObject.transform.rotation);
                    Instantiate(spikes, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - 2, 0f), gameObject.transform.rotation);
                }

            }
          
        }

        Destroy(gameObject);
    }
}