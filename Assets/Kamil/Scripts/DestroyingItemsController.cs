﻿using UnityEngine;
using System.Collections;

public class DestroyingItemsController : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            itemName = gameObject.name;
            Debug.Log(itemName);

            invokeName = itemName.Replace("(Clone)", "");

            itemController.Invoke(invokeName, 0.01f);
            Destroy(gameObject);
        }
    }

    string itemName;
    string invokeName;
    ItemController itemController;

	void Start ()
    {
        itemController = GameObject.Find("GameManager").GetComponent<ItemController>();
    }
}
