﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SpellController : MonoBehaviour {

    Player player;
    Transform transformPlayer;

    BoardManager boardManager;
    ItemController itemController;

    public static SpellController spellController = null;

    public bool isActive;
    public int curCooldown;

    public GameObject[] spellParticles;

    public GameObject[] itemTiles;
    public GameObject[] spellTiles;
    [SerializeField] public GameObject spellSprite;
    [SerializeField] private GameObject[] spellSprites;

	void Start ()
    {
        spellController = this;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        transformPlayer = player.transform;

        boardManager = GameObject.Find("GameManager").GetComponent<BoardManager>();
        itemController = GameObject.Find("GameManager").GetComponent<ItemController>();

        spellSprite.GetComponent<Image>().color = new Color(1f, 1f, 1f, 0f);
	}
	
	void Update ()
    {
        if(player == null)
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

        if (curCooldown == 0 && (Input.GetButtonDown("Fire2")))
        {
            switch (type)
            {
                case (Spell.teleport):

                    isActive = true;

                    break;

                case (Spell.revival):

                    curClockNumber = Clock.turns.countingPlayerMoves;
                    isActive = true;

                    curCooldown += spellCooldown[(int)type];
                    break;

                case (Spell.lastChance):

                    player.StopAllCoroutines();

                    itemController.type = ItemController.Item.nothing;
                    itemController.SwitchingItems();
                    itemController.isActive = false;

                    type = Spell.nothing;
                    SwitchingSpells();
                    isActive = false;

                    boardManager.ClearingMap(null);

                    player.playerStats.curHealth = 1;
                    player.playerStats.curShield = 0;

                    break;

                case (Spell.poison):

                    curClockNumber = Clock.turns.countingPlayerMoves;
                    poisonHolder = new GameObject("poisonHolder").transform;
                    isActive = true;
                    curCooldown += spellCooldown[(int)type];

                    break;

                case (Spell.backstab):

                    isActive = true;

                    break;

                case (Spell.masochism):

                    player.masochismParticles.SetActive(true);
                    curClockNumber = Clock.turns.countingPlayerMoves;
                    isActive = true;

                    break;

                case (Spell.stun):
                    isActive = true;

                    break;

                case (Spell.changingFate):

                    GameObject[] items = GameObject.FindGameObjectsWithTag("Item");
                    GameObject[] spells = GameObject.FindGameObjectsWithTag("Spell");

                    for (int i = 0; i < items.Length; i++)
                    {
                        if (items[i].transform.position.x < player.transform.position.x + 3 && items[i].transform.position.x > player.transform.position.x - 3)
                            if (items[i].transform.position.y < player.transform.position.y + 3 && items[i].transform.position.y > player.transform.position.y - 3)
                            {
                                GameObject instance = Instantiate(itemTiles[Random.Range(0, itemTiles.Length)], items[i].transform.position, items[i].transform.rotation) as GameObject;

                                if (instance.name == items[i].name)
                                {
                                    Destroy(instance);
                                    i--;
                                }

                                else Destroy(items[i]);
                            }
                    }

                    for (int i = 0; i < spells.Length; i++)
                    {
                        if (spells[i].transform.position.x < player.transform.position.x + 3 && spells[i].transform.position.x > player.transform.position.x - 3)
                            if (spells[i].transform.position.y < player.transform.position.y + 3 && spells[i].transform.position.y > player.transform.position.y - 3)
                            {
                                GameObject instance = Instantiate(spellTiles[Random.Range(0, spellTiles.Length)], spells[i].transform.position, spells[i].transform.rotation) as GameObject;

                                if (instance.name == spells[i].name)
                                {
                                    Destroy(instance);
                                    i--;
                                }

                                else Destroy(spells[i]);
                            }
                    }

                    isActive = false;
                    curCooldown += spellCooldown[(int)type];
                    break;

                case (Spell.sacredAttack):

                    isActive = true;

                    BoxCollider2D boxColVer = player.gameObject.AddComponent<BoxCollider2D>(); //Tworzenie pionowego collidera
                    BoxCollider2D boxColHor = player.gameObject.AddComponent<BoxCollider2D>(); //Tworzenie poziomowego collidera

                    GameObject clone = (GameObject)Instantiate(spellParticles[(int)type], boxColHor.transform.position, Quaternion.identity);
                    Destroy(clone, 1f);

                    boxColHor.size = new Vector2(6f, 0.5f);
                    boxColHor.isTrigger = true;

                    boxColVer.size = new Vector2(0.5f, 6f);
                    boxColVer.isTrigger = true;

                    StartCoroutine(SacredAttackDestroyer(boxColHor, boxColVer));
                    curCooldown += spellCooldown[(int)type];

                    break;

                case (Spell.stealth):

                    GameObject[] enemiesForStealth = GameObject.FindGameObjectsWithTag("Enemy");
                    for (int i = 0; i < enemiesForStealth.Length; i++)
                    {
                        enemiesForStealth[i].GetComponent<Enemy>().playerInRange = false;
                    }

                    GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CircleCollider2D>().radius = 3;
                    curPlayerHp = player.playerStats.curHealth;
                    stealthBool = true;

                    break;


            }
        }

        /************************************************/
        #region Castowanie teleportacji
         if (type == Spell.teleport && isActive == true && curCooldown == 0)
         {

            if (Input.GetButtonDown("Horizontal") || Input.GetButtonDown("Vertical"))
            {
                GameObject clone = (GameObject)Instantiate(spellParticles[(int)type], player.transform.position, Quaternion.identity);
                Destroy(clone, 3f);
            }

            if (Input.GetKeyDown(KeyCode.RightArrow))
             {
                 while (isActive)
                 {
                     if (boardManager.wallsPositions.Contains(new Vector3(player.transform.position.x + 1, player.transform.position.y, 0f)))
                     {
                         isActive = false;
                         curCooldown += spellCooldown[(int)type];
                     }
                     else player.transform.position = new Vector3(player.transform.position.x + 1, player.transform.position.y, 0f);
                 }
             }
             else if (Input.GetKeyDown(KeyCode.LeftArrow))
             {
                 while (isActive)
                 {
                     if (boardManager.wallsPositions.Contains(new Vector3(player.transform.position.x - 1, player.transform.position.y, 0f)))
                     {
                         isActive = false;
                         curCooldown += spellCooldown[(int)type];
                     }
                     else player.transform.position = new Vector3(player.transform.position.x - 1, player.transform.position.y, 0f);
                 }
             }
             else if (Input.GetKeyDown(KeyCode.DownArrow))
             {
                 while (isActive)
                 {
                     if (boardManager.wallsPositions.Contains(new Vector3(player.transform.position.x, player.transform.position.y - 1, 0f)))
                     {
                         isActive = false;
                         curCooldown += spellCooldown[(int)type];
                     }
                     else player.transform.position = new Vector3(player.transform.position.x, player.transform.position.y - 1, 0f);
                 }
             }
             else if (Input.GetKeyDown(KeyCode.UpArrow))
             {
                 while (isActive)
                 {
                     if (boardManager.wallsPositions.Contains(new Vector3(player.transform.position.x, player.transform.position.y + 1, 0f)))
                     {
                         isActive = false;
                         curCooldown += spellCooldown[(int)type];
                     }
                     else player.transform.position = new Vector3(player.transform.position.x, player.transform.position.y + 1, 0f);
                 }
             }
         }
        #endregion

        #region Castowanie backstaba
        if (type == Spell.backstab && isActive == true && curCooldown == 0)
        {
            Vector3 savedPlayerPos = player.transform.position; //Pozycja gracza, która będzie oddawana, jeśli nikogo nie zaatakuje

            if(Input.GetButtonDown("Horizontal") || Input.GetButtonDown("Vertical"))
            {
                GameObject clone = (GameObject) Instantiate(spellParticles[(int)type], player.transform.position, Quaternion.identity);
                Destroy(clone, 3f);
            }

            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                CheckingEnemiesPosition();
                while (isActive)
                {
                    if (boardManager.enemiesPositions.Contains(new Vector3(player.transform.position.x + 1, player.transform.position.y, 0f)))
                    {
                        if (boardManager.wallsPositions.Contains(new Vector3(player.transform.position.x + 2, player.transform.position.y, 0f)))
                        {
                            Debug.Log("OK");
                            player.transform.position = savedPlayerPos;
                            isActive = false;
                        }
                        else
                        {
                            player.transform.position = new Vector3(player.transform.position.x + 1, player.transform.position.y, 0f);
                            BackstabingEnemy();

                            isActive = false;
                            curCooldown += spellCooldown[(int)type];
                        }
                    }
                    else player.transform.position = new Vector3(player.transform.position.x + 1, player.transform.position.y, 0f);

                    if (player.transform.position.x >= boardManager.maximumPosition.x)
                    {
                        player.transform.position = savedPlayerPos;
                        isActive = false;
                    }
                }
            }
            else if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                CheckingEnemiesPosition();
                while (isActive)
                {
                    if (boardManager.enemiesPositions.Contains(new Vector3(player.transform.position.x - 1, player.transform.position.y, 0f)))
                    {
                        if (boardManager.wallsPositions.Contains(new Vector3(player.transform.position.x - 2, player.transform.position.y, 0f)))
                        {
                            Debug.Log("OK");
                            player.transform.position = savedPlayerPos;
                            isActive = false;
                        }
                        else
                        {
                            player.transform.position = new Vector3(player.transform.position.x - 1, player.transform.position.y, 0f);
                            BackstabingEnemy();

                            isActive = false;
                            curCooldown += spellCooldown[(int)type];
                        }
                    }
                    else player.transform.position = new Vector3(player.transform.position.x - 1, player.transform.position.y, 0f);

                    if (player.transform.position.x <= boardManager.minimumPosition.x)
                    {
                        player.transform.position = savedPlayerPos;
                        isActive = false;
                    }
                }
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                CheckingEnemiesPosition();
                while (isActive)
                {
                    if (boardManager.enemiesPositions.Contains(new Vector3(player.transform.position.x, player.transform.position.y - 1, 0f)))
                    {
                        if (boardManager.wallsPositions.Contains(new Vector3(player.transform.position.x, player.transform.position.y - 2, 0f)))
                        {
                            Debug.Log("OK");
                            player.transform.position = savedPlayerPos;
                            isActive = false;
                        }
                        else
                        {
                            player.transform.position = new Vector3(player.transform.position.x, player.transform.position.y - 1f, 0f);
                            BackstabingEnemy();

                            isActive = false;
                            curCooldown += spellCooldown[(int)type];
                        }
                    }
                    else player.transform.position = new Vector3(player.transform.position.x, player.transform.position.y - 1, 0f);

                    if (player.transform.position.y <= boardManager.minimumPosition.y)
                    {
                        player.transform.position = savedPlayerPos;
                        isActive = false;
                    }
                }
            }
            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                CheckingEnemiesPosition();
                while (isActive)
                {
                    if (boardManager.enemiesPositions.Contains(new Vector3(player.transform.position.x, player.transform.position.y + 1, 0f)))
                    {
                        if (boardManager.wallsPositions.Contains(new Vector3(player.transform.position.x, player.transform.position.y + 2, 0f)))
                        {
                            Debug.Log("OK");
                            player.transform.position = savedPlayerPos;
                            isActive = false;
                        }
                        else
                        {
                            player.transform.position = new Vector3(player.transform.position.x, player.transform.position.y + 1f, 0f);
                            BackstabingEnemy();

                            isActive = false;
                            curCooldown += spellCooldown[(int)type];
                        }
                    }
                    else player.transform.position = new Vector3(player.transform.position.x, player.transform.position.y + 1, 0f);

                    if (player.transform.position.y >= boardManager.maximumPosition.y)
                    {
                        player.transform.position = savedPlayerPos;
                        isActive = false;
                    }
                }
            }
        }
        #endregion
    }

    void CheckingEnemiesPosition()
    {
        boardManager.enemiesPositions.Clear();
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

        for (int i = 0; i < enemies.Length; i++)
        {
            boardManager.enemiesPositions.Add(enemies[i].transform.position);
        }
    }

    void BackstabingEnemy()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

        for (int i = 0; i < enemies.Length; i++)
            if (player.transform.position == enemies[i].transform.position)
            {
                enemies[i].GetComponent<Enemy>().DamageEnemy(player.playerStats.physicalDamage * 5);
            }
    }

    void FixedUpdate()
    {
        //Debug.Log(isActive);
        //Debug.Log(curCooldown);

        if (swappedSpells)
        {
            if (Clock.turns.countingPlayerMoves > curClock + 1)
            {
                spellOnGround.GetComponent<Collider2D>().enabled = true;
                swappedSpells = false;
            }
        }

        if ((type == Spell.masochism && curCooldown > 0) || type != Spell.masochism)
        {
            player.masochismParticles.SetActive(false);
        }

        if (curCooldown < 0) curCooldown = 0;

        if (isActive)
        {           
            if (curClockNumber + 1 < Clock.turns.countingPlayerMoves && type == Spell.revival && isActive) { isActive = false; } //Oczekiwanie 1 turę aby sprawdzić czy gracz zginie
            else
            {
                if (player.playerStats.curHealth <= 0 && type == Spell.revival && isActive) //Jeśli gracz ginie po użyciu zaklęcia revival wtedy:
                {
                    player.playerStats.curHealth = player.playerStats.magic * 6; //Odzyskanie zdrowia
                    GameObject instance = Instantiate(gameObject.GetComponent<ItemController>().bombToSpawn, player.transform.position, Quaternion.identity) as GameObject; //spawn bomby
                    StartCoroutine(RevivalExplosion(instance)); //Oczekiwanie na wybuch bomby i niszczenie jej oraz isActive = false 
                }
            }                      

            if (curClockNumber + 3 < Clock.turns.countingPlayerMoves && type == Spell.masochism && isActive)//Oczekiwanie 3 turę, aby sprawdzać obrażenia gracza
            {                
                isActive = false;
                curCooldown += spellCooldown[(int)type];
            }            
            else
            {
                if (type == Spell.masochism && isActive)
                {
                    //Nie wiem czy to co robię ma sens...
                }
            }

            

            if (type == Spell.stun && isActive)
            {
                isActive = false;
                curCooldown += spellCooldown[(int)type];

                GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
                for (int i = 0; i < enemies.Length; i++)
                {
                    if (enemies[i] != null)
                    {
                        enemies[i].GetComponent<Enemy>().stunned = (int) (player.playerStats.magicDamage * 0.8f);
                    }
                }
            }

            if (type == Spell.poison && curClockNumber < Clock.turns.countingPlayerMoves && isActive)
            {
                curClockNumber++;

                //GameObject instance = Instantiate(boardManager.floorTiles[Random.Range(0, boardManager.floorTiles.Length)], player.transform.position, Quaternion.identity) as GameObject;
                

                GameObject instance = Instantiate(poisonTile[Random.Range(0, poisonTile.Length)], player.transform.position, Quaternion.identity) as GameObject;
                instance.GetComponent<SpriteRenderer>().color = new Color(0f, 1f, 0f, 1f);
                instance.transform.SetParent(poisonHolder);
            }  
        }

        if (type == Spell.stealth && curCooldown == 0 && stealthBool)
        {
            if (curPlayerHp != player.playerStats.curHealth)
            {
                if (curPlayerHp > player.playerStats.curHealth)
                {
                    curCooldown += spellCooldown[(int)type];
                    GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CircleCollider2D>().radius = 15;
                    GameObject[] enemiesForStealth = GameObject.FindGameObjectsWithTag("Enemy");

                    for (int i = 0; i < enemiesForStealth.Length; i++)
                    {
                        enemiesForStealth[i].GetComponent<Enemy>().playerInRange = false;
                    }
                    stealthBool = false;
                }
                else if (curPlayerHp < player.playerStats.curHealth)
                {
                    curPlayerHp = player.playerStats.curHealth;
                }
            }
            else { /*Gdy nic się nie dzieje ;< */ }
        }
    }



    IEnumerator RevivalExplosion(GameObject bomb)
    {
        bomb.GetComponent<Collider2D>().enabled = true;
        GameObject clone = (GameObject)Instantiate(spellParticles[(int)type], bomb.transform.position, Quaternion.identity);
        Destroy(clone, 3f);

        yield return new WaitForSeconds(0.2f);

        bomb.GetComponent<Collider2D>().enabled = false;
        Destroy(bomb);
        isActive = false;

    }

    IEnumerator SacredAttackDestroyer(BoxCollider2D hor, BoxCollider2D ver)
    {
        yield return new WaitForSeconds(0.05f);
        Destroy(hor);
        Destroy(ver);

        isActive = false;
    }


    /********************** AKTYWACJA SPELLI ETC. *********************/
    //Zmienne dla obsługi zaklęć:
    public enum Spell { nothing, teleport, revival, lastChance, poison, backstab, masochism, stun, changingFate, sacredAttack, stealth }
    public Spell type;
    public int[] spellCooldown = new int[10];
    public int[] curCooldowns = new int[10];

    int curClockNumber;
    float curPlayerHp;
    Transform poisonHolder;
    Vector3 poisonPos;
    [SerializeField] public GameObject[] poisonTile;
    public bool stealthBool; //Dpdatkowy boolean, aby nie występowały błędy z kamerą, gdy jest aktywny stealth
    //Jest to wyjątkowy spell, bo jako jedyny działa do momentu aż gracz otrzyma obrażenia, więc potrzebna jest dodatkowa zmienna
    private bool swappedSpells;
    private GameObject spellOnGround;
    private int curClock;

    /*************** Aktywowanie przedmiotów *************************/
    void Teleport()
    {
        SwitchingSpellsOnGround();
        type = Spell.teleport;
        SwitchingSpells();
    }

    void Revival()
    {
        SwitchingSpellsOnGround();
        type = Spell.revival;
        SwitchingSpells();
    }

    void LastChance()
    {
        SwitchingSpellsOnGround();
        type = Spell.lastChance;
        SwitchingSpells();
    }

    void Poison()
    {
        SwitchingSpellsOnGround();
        type = Spell.poison;
        SwitchingSpells();
    }

    void Backstab()
    {
        SwitchingSpellsOnGround();
        type = Spell.backstab;
        SwitchingSpells();
    }

    void Masochism()
    {
        SwitchingSpellsOnGround();
        type = Spell.masochism;
        SwitchingSpells();
    }

    void Stun()
    {
        SwitchingSpellsOnGround();
        type = Spell.stun;
        SwitchingSpells();
    }

    void ChangingFate()
    {
        SwitchingSpellsOnGround();
        type = Spell.changingFate;
        SwitchingSpells();
    }

    void SacredAttack()
    {
        SwitchingSpellsOnGround();
        type = Spell.sacredAttack;
        SwitchingSpells();
    }

    void Stealth()
    {
        SwitchingSpellsOnGround();
        type = Spell.stealth;
        SwitchingSpells();
    }

    //
    void SwitchingSpells()
    {
        curCooldown = curCooldowns[(int)type];
        isActive = false;

        if (type == Spell.nothing) spellSprite.GetComponent<Image>().color = new Color(1f, 1f, 1f, 0f);
        else
        {
            spellSprite.GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);
            spellSprite.GetComponent<Image>().sprite = spellSprites[(int)type].GetComponent<SpriteRenderer>().sprite;
        }
    }

    void SwitchingSpellsOnGround()
    {
        if (type == Spell.nothing)
            return;
        else
        {
            curCooldowns[(int)type] = curCooldown;
            spellOnGround = (GameObject)Instantiate(spellSprites[(int)type], new Vector3((int)player.transform.position.x, (int)player.transform.position.y, (int)player.transform.position.z), Quaternion.identity);
            spellOnGround.GetComponent<Collider2D>().enabled = false;
            curClock = Clock.turns.countingPlayerMoves;
            swappedSpells = true;
        }
    }
}