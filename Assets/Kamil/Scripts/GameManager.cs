﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    private BoardManager boardManager;
    [HideInInspector]
    public int level = 1;

    private GameObject player;
    public GameObject levelScreen;
    public GameObject deathScreen;

    public float levelStartDelay;
    public float deathDelay;
    public bool doingSetup;
    public bool rip;


    void Awake()
    {
        if (instance == null)
            instance = this;

        else if (instance != null)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
        boardManager = GetComponent<BoardManager>();
        InitializeGame();
    }

    void Update()
    {    
        if (player == null)
            player = GameObject.FindGameObjectWithTag("Player");

        if (doingSetup || rip)
            return;

        if (Input.GetButtonDown("Restart"))
        {
            player.GetComponent<Player>().StopAllCoroutines();
            RestartLevel();
        }
    }

    public void RestartLevel()
    {
        rip = true;
        doingSetup = true;
        boardManager.ClearingMap(null);
    }

    public void InvokeGameOver()
    {
        doingSetup = true;
        deathScreen.GetComponent<Animator>().SetTrigger("Death");
        Invoke("GameOver", deathDelay);        
    }

    void GameOver()
    {
        player.GetComponent<Player>().sprite.gameObject.SetActive(true);
        player.transform.position = transform.position = new Vector3(0f, 4f, 0f);
        RestartLevel();        
    }

    public void InitializeGame()
    {
        levelScreen.SetActive(true);

        levelScreen.transform.FindChild("Text").GetComponent<Text>().text = "Poziom " + level;

        doingSetup = true;

        Invoke("LevelStart", levelStartDelay);

        Clock.turns.enemies.Clear();

        boardManager.SetupScene(level);
    }

    private void LevelStart()
    {
        doingSetup = false;
        levelScreen.SetActive(false);
    }
}