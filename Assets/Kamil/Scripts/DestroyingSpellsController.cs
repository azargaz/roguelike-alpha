﻿using UnityEngine;
using System.Collections;

public class DestroyingSpellsController : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            itemName = gameObject.name;
            Debug.Log(itemName);

            invokeName = itemName.Replace("(Clone)", "");

            spellController.Invoke(invokeName, 0.01f);
            Destroy(gameObject);
        }
    }

    string itemName;
    string invokeName;
    SpellController spellController;

    void Start()
    {
        spellController = GameObject.Find("GameManager").GetComponent<SpellController>();
    }
}