﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class BoardManager : MonoBehaviour
{
    [Serializable]
    public class Count
    {
        public int maximum;
        public int minimum;

        public Count(int min, int max)
        {
            minimum = min;
            maximum = max;
        }
    }

    public int xAxis; //ilość pól względem osi X (szerokość)
    public int yAxis; //ilość pól względem osi Y (wysokość)

    public GameObject[] floorTiles; //tablica przechowująca różne rodzaje podłóg
    public GameObject[] wallTiles; //tablica przechowująca różne rodzaje ścian
    public GameObject[] itemTiles; //tablica przechowująca różne przedmioty
    public GameObject[] enemyTiles; //tablica przechowująca różne rodzaje przeciwników
    public GameObject[] corridorTiles; //tablica przechowująca podłogi dla korytarzy
    public GameObject[] trapTiles; //tablica przechowująca różne rodzaje pułapek

    public Count itemCount = new Count(1, 5); //Ilość przedmiotów na mapie (na razie tylko skrzynie --17.02.2016)
    public Count enemyCount = new Count(1, 5); //Ilość przeciwników na mapie

    [SerializeField] GameObject Player;

    private Transform boardHolder; //Obiekt w którym przechowywane są tworzone pola, odpowiada jednego pokojowi
    [HideInInspector] public List<Vector3> gridPositions = new List<Vector3>(); //Lista pozycji wszystkich spawnowanych pól
    [HideInInspector] public List<Vector3> floorPositions = new List<Vector3>();
    [HideInInspector] public List<Vector3> wallsPositions = new List<Vector3>();
    [HideInInspector] public List<Vector3> enemiesPositions = new List<Vector3>();
    [HideInInspector] public List<Vector3> corridorPositions = new List<Vector3>();
    [HideInInspector] public List<Vector3> chestPositions = new List<Vector3>();


    public Vector3 maximumPosition; //Pozycja pola maksymalnie wysuniętego względem osi XY
    public Vector3 minimumPosition; //Pozycja pola minimalnie wysuniętego względem osi XY

    int posX; //Pozycja na której zakończyło się spawnowanie obiektu; Od tego miejsca rozpoczyna się kolejne spawnowanie alejek i pokoi
    int posY; //Pozycja na której zakończyło się spawnowanie obiektu; Od tego miejsca rozpoczyna się kolejne spawnowanie alejek i pokoi

    int getX; //Przedłużenie posX, przejmuje pozycje x po zakończeniu spawnowania alejki
    int getY; //Przedłużenie posY, przejmuje pozycje y po zakończeniu spawnowania alejki

    [SerializeField] GameObject exit;

    int[] enemiesPacksSpawned = new int[10];
    /**********************************************************************************************************************************/


    //W obecnym kontekście zbędne, wyrzucone po przerobieniu kodu - 07.02.2016-----
    void InitialiseList()
    {
        gridPositions.Clear();

        for (int x = posX - xAxis; x < posX; x++)
        {
            //Debug.Log("x = " + x);
            for (int y = posY - yAxis; y < posY; y++)
            {
                //Debug.Log("y = " + y);
                gridPositions.Add(new Vector3(x, y, 0f));
            }
        }
    }

    void SpawningEnemies()
    {
        int randomSpawn = 0;
        if (gameObject.GetComponent<GameManager>().level == 1)
            randomSpawn = Random.Range(0, 4);
        else if (gameObject.GetComponent<GameManager>().level == 2)
            randomSpawn = Random.Range(0, 4);
        else if (gameObject.GetComponent<GameManager>().level == 3)
            randomSpawn = Random.Range(0, 8);
        else if (gameObject.GetComponent<GameManager>().level == 4)
            randomSpawn = Random.Range(0, 8);
        else if (gameObject.GetComponent<GameManager>().level >= 5)
            randomSpawn = Random.Range(0, 10);

        //EnemyTile:
        //0 - slime
        //1 - kamieniarz
        //2 - kret
        //3 - mag
        int n; //Zmienna pomocnicza do zmiany liczby spawnowanych przeciwników w stosunku pierwszy-drugi level. Potem nieużywane
        int m; //↑↑↑ - dodatkowa zmienna w przyadku dwóch rodzai przeciwników na pokój

        enemiesPacksSpawned[randomSpawn] += 1; //Pamiętać o zclearowaniu tego na końcu
     //   if (enemiesPacksSpawned[randomSpawn] > 2)
     //   {
            while (enemiesPacksSpawned[randomSpawn] > 3)
            {
                randomSpawn = (randomSpawn + 1) % 9;
            }
        // }
        //Debug.Log(randomSpawn);
        #region SWITCH
        switch (randomSpawn)
        {
            case (0): //Spawnowanie tylko slime'ów
                if (gameObject.GetComponent<GameManager>().level == 1) n = Random.Range(1, 3); //Slime
                  else n = Random.Range(2, 4);

                for (int i = 0; i <= n; i++)
                {
                    Vector3 enemyPos = new Vector3((int)(Random.Range(gridPositions[0].x + 1, gridPositions[gridPositions.Count - 1].x - 1)),
                            (int)Random.Range(gridPositions[0].y + 1, gridPositions[gridPositions.Count - 1].y - 1), 0f);

                    if (!enemiesPositions.Contains(enemyPos) && enemyPos != new Vector3(0f, 4f, 0f))
                    {
                        Instantiate(enemyTiles[0], enemyPos, Quaternion.identity);
                        enemiesPositions.Add(enemyPos);
                    }
                    else i--;
                }
                /******/
                break;


            case (1): //Spawnowanie tylko kamieniarzy
                if (gameObject.GetComponent<GameManager>().level == 1) n = Random.Range(0, 2); //Kamieniarz
                    else n = Random.Range(0, 3);

                for (int i = 0; i <= n; i++) 
                {
                    Vector3 enemyPos = new Vector3((int)(Random.Range(gridPositions[0].x + 1, gridPositions[gridPositions.Count - 1].x - 1)),
                            (int)Random.Range(gridPositions[0].y + 1, gridPositions[gridPositions.Count - 1].y - 1), 0f);

                    if (!enemiesPositions.Contains(enemyPos) && enemyPos != new Vector3(0f, 4f, 0f))
                    {
                        Instantiate(enemyTiles[1], enemyPos, Quaternion.identity);
                        enemiesPositions.Add(enemyPos);
                    }
                    else i--;
                }
                /******/
                break;

            case (2): //Spawnowanie slime'ów i kamieniarzy
                if (gameObject.GetComponent<GameManager>().level == 1) n = Random.Range(0, 1); //Slime
                    else n = Random.Range(0, 2);

                if (gameObject.GetComponent<GameManager>().level == 1) m = Random.Range(0, 2); //Kamieniarz
                  else m = Random.Range(1, 3);

                for (int i = 0; i <= n; i++) //Spawnowanie slime'ów
                {
                    Vector3 enemyPos = new Vector3((int)(Random.Range(gridPositions[0].x + 1, gridPositions[gridPositions.Count - 1].x - 1)),
                            (int)Random.Range(gridPositions[0].y + 1, gridPositions[gridPositions.Count - 1].y - 1), 0f);

                    if (!enemiesPositions.Contains(enemyPos) && enemyPos != new Vector3(0f, 4f, 0f))
                    {
                        Instantiate(enemyTiles[0], enemyPos, Quaternion.identity);
                        enemiesPositions.Add(enemyPos);
                    }
                    else i--;
                }

                for (int i = 0; i <= m; i++) //Spawnowanie kamieniarzy
                {
                    Vector3 enemyPos = new Vector3((int)(Random.Range(gridPositions[0].x + 1, gridPositions[gridPositions.Count - 1].x - 1)),
                            (int)Random.Range(gridPositions[0].y + 1, gridPositions[gridPositions.Count - 1].y - 1), 0f);

                    if (!enemiesPositions.Contains(enemyPos) && enemyPos != new Vector3(0f, 4f, 0f))
                    {
                        Instantiate(enemyTiles[1], enemyPos, Quaternion.identity);
                        enemiesPositions.Add(enemyPos);
                    }
                    else i--;
                }
                /******/
                break;

            case (3): //Spawnowanie kreta i slime'ów
                if (gameObject.GetComponent<GameManager>().level == 1) n = Random.Range(0, 1); //Slime
                else n = Random.Range(1, 3);

                if (gameObject.GetComponent<GameManager>().level == 1) m = Random.Range(0, 0); //Kret
                else m = Random.Range(0, 0);

                for (int i = 0; i <= n; i++) //Spawnowanie slime'ów
                {
                    Vector3 enemyPos = new Vector3((int)(Random.Range(gridPositions[0].x + 1, gridPositions[gridPositions.Count - 1].x - 1)),
                            (int)Random.Range(gridPositions[0].y + 1, gridPositions[gridPositions.Count - 1].y - 1), 0f);

                    if (!enemiesPositions.Contains(enemyPos) && enemyPos != new Vector3(0f, 4f, 0f))
                    {
                        Instantiate(enemyTiles[0], enemyPos, Quaternion.identity);
                        enemiesPositions.Add(enemyPos);
                    }
                    else i--;
                }

                for (int i = 0; i <= m; i++) //Spawnowanie kreta
                {
                    Vector3 enemyPos = new Vector3((int)(Random.Range(gridPositions[0].x + 1, gridPositions[gridPositions.Count - 1].x - 1)),
                            (int)Random.Range(gridPositions[0].y + 1, gridPositions[gridPositions.Count - 1].y - 1), 0f);

                    if (!enemiesPositions.Contains(enemyPos) && enemyPos != new Vector3(0f, 4f, 0f))
                    {
                        Instantiate(enemyTiles[2], enemyPos, Quaternion.identity);
                        enemiesPositions.Add(enemyPos);
                    }
                    else i--;
                }
                /******/
                break;

            case (4):
                for (int i = 0; i <= Random.Range(0, 2); i++) //Spawnowanie magów
                {
                    Vector3 enemyPos = new Vector3((int)(Random.Range(gridPositions[0].x + 1, gridPositions[gridPositions.Count - 1].x - 1)),
                            (int)Random.Range(gridPositions[0].y + 1, gridPositions[gridPositions.Count - 1].y - 1), 0f);

                    if (!enemiesPositions.Contains(enemyPos) && enemyPos != new Vector3(0f, 4f, 0f))
                    {
                        Instantiate(enemyTiles[3], enemyPos, Quaternion.identity);
                        enemiesPositions.Add(enemyPos);
                    }
                    else i--;
                }
                /******/
                break;


            case (5)://Spawnowanie magów i kamieniarzy
                for (int i = 0; i <= Random.Range(0, 1); i++)  //Spawnowanie magów
                {
                    Vector3 enemyPos = new Vector3((int)(Random.Range(gridPositions[0].x + 1, gridPositions[gridPositions.Count - 1].x - 1)),
                            (int)Random.Range(gridPositions[0].y + 1, gridPositions[gridPositions.Count - 1].y - 1), 0f);

                    if (!enemiesPositions.Contains(enemyPos) && enemyPos != new Vector3(0f, 4f, 0f))
                    {
                        Instantiate(enemyTiles[3], enemyPos, Quaternion.identity);
                        enemiesPositions.Add(enemyPos);
                    }
                    else i--;
                }

                for (int i = 0; i <= Random.Range(0, 2); i++)  //Spawnowanie kamieniarzy
                {
                    Vector3 enemyPos = new Vector3((int)(Random.Range(gridPositions[0].x + 1, gridPositions[gridPositions.Count - 1].x - 1)),
                            (int)Random.Range(gridPositions[0].y + 1, gridPositions[gridPositions.Count - 1].y - 1), 0f);

                    if (!enemiesPositions.Contains(enemyPos) && enemyPos != new Vector3(0f, 4f, 0f))
                    {
                        Instantiate(enemyTiles[1], enemyPos, Quaternion.identity);
                        enemiesPositions.Add(enemyPos);
                    }
                    else i--;
                }
                /******/
                break;

            case (6)://Spawnowanie magów i kretów
                for (int i = 0; i <= Random.Range(0, 2); i++)  //Spawnowanie magów
                {
                    Vector3 enemyPos = new Vector3((int)(Random.Range(gridPositions[0].x + 1, gridPositions[gridPositions.Count - 1].x - 1)),
                            (int)Random.Range(gridPositions[0].y + 1, gridPositions[gridPositions.Count - 1].y - 1), 0f);

                    if (!enemiesPositions.Contains(enemyPos) && enemyPos != new Vector3(0f, 4f, 0f))
                    {
                        Instantiate(enemyTiles[3], enemyPos, Quaternion.identity);
                        enemiesPositions.Add(enemyPos);
                    }
                    else i--;
                }

                for (int i = 0; i <= Random.Range(0, 0); i++)  //Spawnowanie kretów
                {
                    Vector3 enemyPos = new Vector3((int)(Random.Range(gridPositions[0].x + 1, gridPositions[gridPositions.Count - 1].x - 1)),
                            (int)Random.Range(gridPositions[0].y + 1, gridPositions[gridPositions.Count - 1].y - 1), 0f);

                    if (!enemiesPositions.Contains(enemyPos) && enemyPos != new Vector3(0f, 4f, 0f))
                    {
                        Instantiate(enemyTiles[2], enemyPos, Quaternion.identity);
                        enemiesPositions.Add(enemyPos);
                    }
                    else i--;
                }
                /******/
                break;

            case (7)://Spawnowanie magów i slime'ów
                for (int i = 0; i <= Random.Range(0, 2); i++)  //Spawnowanie magów
                {
                    Vector3 enemyPos = new Vector3((int)(Random.Range(gridPositions[0].x + 1, gridPositions[gridPositions.Count - 1].x - 1)),
                            (int)Random.Range(gridPositions[0].y + 1, gridPositions[gridPositions.Count - 1].y - 1), 0f);

                    if (!enemiesPositions.Contains(enemyPos) && enemyPos != new Vector3(0f, 4f, 0f))
                    {
                        Instantiate(enemyTiles[3], enemyPos, Quaternion.identity);
                        enemiesPositions.Add(enemyPos);
                    }
                    else i--;
                }

                for (int i = 0; i <= Random.Range(1, 2); i++)  //Spawnowanie slime'ów
                {
                    Vector3 enemyPos = new Vector3((int)(Random.Range(gridPositions[0].x + 1, gridPositions[gridPositions.Count - 1].x - 1)),
                            (int)Random.Range(gridPositions[0].y + 1, gridPositions[gridPositions.Count - 1].y - 1), 0f);

                    if (!enemiesPositions.Contains(enemyPos) && enemyPos != new Vector3(0f, 4f, 0f))
                    {
                        Instantiate(enemyTiles[0], enemyPos, Quaternion.identity);
                        enemiesPositions.Add(enemyPos);
                    }
                    else i--;
                }
                /******/
                break;

            case (8)://Spawnowanie kretów i kamieniarzy
                for (int i = 0; i <= Random.Range(0, 1); i++)  //Spawnowanie kretów
                {
                    Vector3 enemyPos = new Vector3((int)(Random.Range(gridPositions[0].x + 1, gridPositions[gridPositions.Count - 1].x - 1)),
                            (int)Random.Range(gridPositions[0].y + 1, gridPositions[gridPositions.Count - 1].y - 1), 0f);

                    if (!enemiesPositions.Contains(enemyPos) && enemyPos != new Vector3(0f, 4f, 0f))
                    {
                        Instantiate(enemyTiles[2], enemyPos, Quaternion.identity);
                        enemiesPositions.Add(enemyPos);
                    }
                    else i--;
                }

                for (int i = 0; i <= Random.Range(0, 2); i++)  //Spawnowanie kamieniarzy
                {
                    Vector3 enemyPos = new Vector3((int)(Random.Range(gridPositions[0].x + 1, gridPositions[gridPositions.Count - 1].x - 1)),
                            (int)Random.Range(gridPositions[0].y + 1, gridPositions[gridPositions.Count - 1].y - 1), 0f);

                    if (!enemiesPositions.Contains(enemyPos) && enemyPos != new Vector3(0f, 4f, 0f))
                    {
                        Instantiate(enemyTiles[1], enemyPos, Quaternion.identity);
                        enemiesPositions.Add(enemyPos);
                    }
                    else i--;
                }
                /******/
                break;

            case (9):
                for (int i = 0; i <= Random.Range(0, 3); i++)  //Spawnowanie kretów
                {
                    Vector3 enemyPos = new Vector3((int)(Random.Range(gridPositions[0].x + 1, gridPositions[gridPositions.Count - 1].x - 1)),
                            (int)Random.Range(gridPositions[0].y + 1, gridPositions[gridPositions.Count - 1].y - 1), 0f);

                    if (!enemiesPositions.Contains(enemyPos) && enemyPos != new Vector3(0f, 4f, 0f))
                    {
                        Instantiate(enemyTiles[2], enemyPos, Quaternion.identity);
                        enemiesPositions.Add(enemyPos);
                    }
                    else i--;
                }
                /******/
                break;

        }
        #endregion

        SpawningTraps();
    }

    void SpawningTraps()
    {
        //TrapTile:
        //0 - kolce
        //1 - kolce ze ścian w pokoju
        //2 - kolce blokujące w korytarzu (spawn gdzie indziej w metodzie spawnującej korytarze)
        //3 - klątwa
        //4 - ognie na ziemii

        /* Niedziałające kolce ze ścian
        for (int i = 0; i <= 1; i++)
        {
            Vector3 trapPos = new Vector3((int)(Random.Range(gridPositions[0].x + 1, gridPositions[gridPositions.Count - 1].x - 1)),
                    (int)Random.Range(gridPositions[0].y + 1, gridPositions[gridPositions.Count - 1].y - 1), 0f);

            if (!enemiesPositions.Contains(trapPos) && trapPos != new Vector3(0f, 4f, 0f))
                if (wallsPositions.Contains(new Vector3(trapPos.x + 1, trapPos.y, 0f)) || wallsPositions.Contains(new Vector3(trapPos.x - 1, trapPos.y, 0f))
                    || wallsPositions.Contains(new Vector3(trapPos.x, trapPos.y + 1, 0f)) || wallsPositions.Contains(new Vector3(trapPos.x, trapPos.y - 1, 0f)))
                {
                    {
                        Instantiate(trapTiles[1], trapPos, Quaternion.identity);
                        enemiesPositions.Add(trapPos);
                    }
                }
                else i--;
        }*/


        int randomNumber = 0;
        int chance = Random.Range(0, 100);

        if (chance >= 0 && chance <= 50) //50% szans na kolce
            randomNumber = 0;
        else if (chance >= 51 && chance <= 60) //10% szans na klątwę
            randomNumber = 1;
        else if (chance >= 61 && chance <= 85) //25% szans na ognie
            randomNumber = 2;

        else randomNumber = 3; //15% szans że pokój nie będzie miał pułapek


        switch (randomNumber)
        {
            case (0):

                for (int i = 0; i <= 3; i++)
                {
                    Vector3 trapPos = new Vector3((int)(Random.Range(gridPositions[0].x + 1, gridPositions[gridPositions.Count - 1].x - 1)),
                            (int)Random.Range(gridPositions[0].y + 1, gridPositions[gridPositions.Count - 1].y - 1), 0f);

                    if (!enemiesPositions.Contains(trapPos) && trapPos != new Vector3(0f, 4f, 0f))
                    {
                        Instantiate(trapTiles[0], trapPos, Quaternion.identity);
                        enemiesPositions.Add(trapPos);
                    }
                    else i--;
                }

                break;

            case (1):

                for (int i = 0; i <= 0; i++)
                {
                    Vector3 trapPos = new Vector3((int)(Random.Range(gridPositions[0].x + 1, gridPositions[gridPositions.Count - 1].x - 1)),
                            (int)Random.Range(gridPositions[0].y + 1, gridPositions[gridPositions.Count - 1].y - 1), 0f);

                    if (!enemiesPositions.Contains(trapPos) && trapPos != new Vector3(0f, 4f, 0f))
                    {
                        Instantiate(trapTiles[3], trapPos, Quaternion.identity);
                        enemiesPositions.Add(trapPos);
                    }
                    else i--;
                }

                break;

            case (2):

                SpawningFireTrap();
                break;

            case (3):
                    //NIC KUHWA!
               break;    
        }
    }

    void BoardSetup(int getX, int getY, int dir, int roomNumber) //Tworzenie pojedynczego pokoju
    {
        gridPositions.Clear();
        boardHolder = new GameObject("Room" + roomNumber.ToString()).transform; //Tworzenie obiektu o nazwie board, w którym przechowywane będą pola
        boardHolder.tag = "Holder";
        //Debug.Log(roomNumber);

        xAxis = Random.Range(5, 15);
        yAxis = Random.Range(5, 15);

        if (roomNumber == 0)
        {
            for (int x = getX - xAxis / 2 - 1; x <= getX + (xAxis / 2) + 1; x++)
            {
                for (int y = -1 + getY; y < yAxis + getY + 1; y++)
                {
                    GameObject toInstantiate = floorTiles[Random.Range(0, floorTiles.Length)];

                    if (x == getX - xAxis / 2 - 1 || x == getX + (xAxis / 2) + 1 || y == -1 + getY || y == yAxis + getY)
                    {
                        toInstantiate = wallTiles[Random.Range(0, wallTiles.Length)];
                        wallsPositions.Add(new Vector3(x, y, 0f));
                    }


                    GameObject instance = Instantiate(toInstantiate, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;

                    if (instance.name.Contains("Floor"))
                        floorPositions.Add(new Vector3(x, y, 0f));
                    instance.transform.SetParent(boardHolder);

                    posX = x;
                    posY = y;

                    gridPositions.Add(new Vector3(x, y, 0f));

                    GettingMaximumMinimumPositions(new Vector3(x, y, 0f));
                }
            }
        }

        else if (dir == 0)// && getX != 0 && getY != 0) //Góra
        {
            for (int x = getX - xAxis / 2 - 1; x <= getX + (xAxis / 2) + 1; x++)
            {
                for (int y = -1 + getY; y < yAxis + getY + 1; y++)
                {
                    GameObject toInstantiate = floorTiles[Random.Range(0, floorTiles.Length)];

                    if (x == getX - xAxis / 2 - 1 || x == getX + (xAxis / 2) + 1 || y == -1 + getY || y == yAxis + getY)
                    {
                        toInstantiate = wallTiles[Random.Range(0, wallTiles.Length)];
                        wallsPositions.Add(new Vector3(x, y, 0f));
                    }

                    else floorPositions.Add(new Vector3(x, y, 0f));

                    GameObject instance = Instantiate(toInstantiate, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;

                    if (instance.name.Contains("Floor"))
                        floorPositions.Add(new Vector3(x, y, 0f));
                    instance.transform.SetParent(boardHolder);

                    posX = x;
                    posY = y;

                    gridPositions.Add(new Vector3(x, y, 0f));
                    GettingMaximumMinimumPositions(new Vector3(x, y, 0f));
                }
            }
            SpawningEnemies();
        }

        else if (dir == 2) //Dół
        {
            //for (int x = -1 + getX - posX; x < xAxis + getX - posX + 1; x++)
            for (int x = getX - xAxis / 2 - 1; x <= getX + xAxis / 2 + 1; x++)
            {
                for (int y = getY - yAxis - 1; y < getY + 1; y++)
                {
                    GameObject toInstantiate = floorTiles[Random.Range(0, floorTiles.Length)];

                    if ((x == getX - xAxis / 2 - 1 || x == getX + xAxis / 2 + 1 || y == getY - yAxis - 1 || y == getY))
                    {
                        toInstantiate = wallTiles[Random.Range(0, wallTiles.Length)];
                        wallsPositions.Add(new Vector3(x, y, 0f));
                    }
                    GameObject instance = Instantiate(toInstantiate, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;

                    if (instance.name.Contains("Floor"))
                        floorPositions.Add(new Vector3(x, y, 0f));
                    instance.transform.SetParent(boardHolder);

                    posX = x;
                    posY = y;

                    gridPositions.Add(new Vector3(x, y, 0f));
                    GettingMaximumMinimumPositions(new Vector3(x, y, 0f));
                }
            }
            SpawningEnemies();
        }

        else if (dir == 1) //Prawo
        {
            for (int x = getX; x <= getX + xAxis + 1; x++)
            {
                for (int y = getY - yAxis / 2 - 1; y <= getY + yAxis / 2 + 1; y++)
                {
                    GameObject toInstantiate = floorTiles[Random.Range(0, floorTiles.Length)];

                    if (x == getX || x == xAxis + getX + 1 || y == getY - yAxis / 2 - 1 || y == getY + yAxis / 2 + 1)
                    {
                        toInstantiate = wallTiles[Random.Range(0, wallTiles.Length)];
                        wallsPositions.Add(new Vector3(x, y, 0f));
                    }
                    GameObject instance = Instantiate(toInstantiate, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;

                    if (instance.name.Contains("Floor"))
                        floorPositions.Add(new Vector3(x, y, 0f));
                    instance.transform.SetParent(boardHolder);

                    posX = x;
                    posY = y;

                    gridPositions.Add(new Vector3(x, y, 0f));
                    GettingMaximumMinimumPositions(new Vector3(x, y, 0f));
                }
            }
            SpawningEnemies();
        }
    }
    /******************************** Metody niezwiązane ze spawnowaniem pokoi *************************************/
    void DestroyingWalls()
    {
        GameObject[] walls = GameObject.FindGameObjectsWithTag("Wall");
        for (int i = 0; i < walls.Length; i++)
        {
            for (int j = 0; j < floorPositions.Count; j++)
            {
                if (walls[i].transform.position == floorPositions[j])
                {
                    Destroy(walls[i]);
                    wallsPositions.Remove(walls[i].transform.position);
                }
            }
        }
    }

    void GettingMaximumMinimumPositions(Vector3 curPosition)
    {
        if (curPosition.x > maximumPosition.x) maximumPosition.x = curPosition.x;
        else if (curPosition.x < minimumPosition.x) minimumPosition.x = curPosition.x;

        if (curPosition.y > maximumPosition.y) maximumPosition.y = curPosition.y;
        else if (curPosition.y < minimumPosition.y) minimumPosition.y = curPosition.y;

        else { }
    }

    void FillingPlacesOnTheMap()
    {
        Transform fillHolder = new GameObject("FillHolder").transform;
        fillHolder.tag = "Holder";

        for (int x = (int)minimumPosition.x - 20; x < maximumPosition.x + 20; x++)
        {
            for (int y = (int)minimumPosition.y - 20; y < maximumPosition.y + 20; y++)
            {
                GameObject toInstantiate = wallTiles[Random.Range(0, wallTiles.Length)];

                GameObject instance = Instantiate(toInstantiate, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;

                instance.transform.SetParent(fillHolder);
            }
        }
    }

    void DestroyingCorridorTiles()
    {
        GameObject[] corridorFloors = GameObject.FindGameObjectsWithTag("Floor");

        for (int i = 0; i < corridorFloors.Length; i++)
        {
            if (corridorFloors[i].name.Contains("CorridorFloor"))
            {
                if (wallsPositions.Contains(new Vector3(corridorFloors[i].transform.position.x, corridorFloors[i].transform.position.y + 1, 0f)) &&
                    wallsPositions.Contains(new Vector3(corridorFloors[i].transform.position.x, corridorFloors[i].transform.position.y - 1, 0f)))
                { }
                else if (wallsPositions.Contains(new Vector3(corridorFloors[i].transform.position.x + 1, corridorFloors[i].transform.position.y, 0f)) &&
                    wallsPositions.Contains(new Vector3(corridorFloors[i].transform.position.x - 1, corridorFloors[i].transform.position.y, 0f)))
                { }
                else
                {
                    Destroy(corridorFloors[i]);
                    corridorPositions.Remove(corridorFloors[i].transform.position);
                }
            }
        }
    }

    Vector3 RandomPosition()
    {
        //gridPositions.Clear();

        int randomIndex = Random.Range(0, floorPositions.Count);
        Vector3 randomPosition = floorPositions[randomIndex];
        floorPositions.RemoveAt(randomIndex);

            return randomPosition;
    }

    void RandomSpawning(GameObject[] tileArray, int minimum, int maximum, string type)
    {
        int objectCount = Random.Range(minimum, maximum + 1);
        
        if (type == "Enemy")
        {
            for (int i = 0; i < objectCount; i++)
            {
                Vector3 randomPosition = RandomPosition();
                GameObject tileChoice = tileArray[Random.Range(0, tileArray.Length)];
                if (randomPosition != Player.transform.position)
                {
                    Instantiate(tileChoice, randomPosition, Quaternion.identity);
                }
            }
        }
        else if (type == "Chest")
            for (int i = 0; i < objectCount; i++)
            {
                Vector3 randomPosition = RandomPosition();
                GameObject tileChoice = tileArray[Random.Range(0, tileArray.Length)];

                if (!chestPositions.Contains(randomPosition) && !enemiesPositions.Contains(randomPosition))
                {
                    Instantiate(tileChoice, randomPosition, Quaternion.identity);
                    chestPositions.Add(randomPosition);
                }
                else
                {
                    i--;
                }
            }

    }


    void AlleySpawning(int dir, int roomNumber) //Spawmowanie alejek między kolejnymi pokojami
    {
        Transform corridorHolder = new GameObject("CorridorHolder" + roomNumber).transform; //Holder do przechowywania korytarzy
        corridorHolder.tag = "Holder";

        gridPositions.Clear();
        if (dir == 0) // Góra
        {
            for (int x = posX; x < posX + 1; x++)
            {
                for (int y = posY; y < posY + 5; y++)
                {
                    floorPositions.Add(new Vector3(x, y, 0f));
                    gridPositions.Add(new Vector3(x, y, 0f));

                    GameObject toInstantiate = corridorTiles[Random.Range(0, corridorTiles.Length)];
                    GameObject instance = Instantiate(toInstantiate, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;
                    instance.transform.SetParent(corridorHolder);
                    
                    getX = x;
                    getY = y;
                }
            }
        }

        else if (dir == 2) // Dół
        {
            for (int x = posX; x < posX + 1; x++)
            {
                for (int y = posY; y > posY - 5; y--)
                {
                    floorPositions.Add(new Vector3(x, y, 0f));
                    gridPositions.Add(new Vector3(x, y, 0f));

                    GameObject toInstantiate = corridorTiles[Random.Range(0, corridorTiles.Length)];
                    GameObject instance = Instantiate(toInstantiate, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;
                    instance.transform.SetParent(corridorHolder);

                    getX = x;
                    getY = y;
                }
            }
        }

        else if (dir == 1) // Prawo
        {
            for (int x = posX; x < posX + 5; x++)
            {
                for (int y = posY; y < posY + 1; y++)
                {
                    floorPositions.Add(new Vector3(x, y, 0f));
                    gridPositions.Add(new Vector3(x, y, 0f));

                    GameObject toInstantiate = corridorTiles[Random.Range(0, corridorTiles.Length)];
                    GameObject instance = Instantiate(toInstantiate, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;
                    instance.transform.SetParent(corridorHolder);

                    getX = x;
                    getY = y;
                }
            }
        }
        /********************** SPAWNOWANIE ŚCIAN BLOKUJĄCYCH WYJŚCIE POZA MAPĘ (KORYTARZE) *********************/
        if (dir == 0)
        {
            for (int i = 0; i < gridPositions.Count - 1; i++)
            {
                GameObject toInstantiate = wallTiles[Random.Range(0, wallTiles.Length)];
                Vector3 position = gridPositions[i];
                position.x++;

                GameObject instance = Instantiate(toInstantiate, position, Quaternion.identity) as GameObject;
                wallsPositions.Add(position);
                position.x -= 2;

                GameObject instanceNr2 = Instantiate(toInstantiate, position, Quaternion.identity) as GameObject;
                wallsPositions.Add(position);
            }
        }

        else if (dir == 2)
        {
            for (int i = 1; i < gridPositions.Count; i++)
            {
                GameObject toInstantiate = wallTiles[Random.Range(0, wallTiles.Length)];
                Vector3 position = gridPositions[i];
                position.x++;

                GameObject instance = Instantiate(toInstantiate, position, Quaternion.identity) as GameObject;
                wallsPositions.Add(position);

                position.x -= 2;

                GameObject instanceNr2 = Instantiate(toInstantiate, position, Quaternion.identity) as GameObject;
                wallsPositions.Add(position);
            }
        }

        else if (dir == 1)
        {
            for (int i = 0; i < gridPositions.Count; i++)
            {
                GameObject toInstantiate = wallTiles[Random.Range(0, wallTiles.Length)];
                Vector3 position = gridPositions[i];
                position.y++;

                GameObject instance = Instantiate(toInstantiate, position, Quaternion.identity) as GameObject;
                wallsPositions.Add(position);

                position.y -= 2;

                GameObject instanceNr2 = Instantiate(toInstantiate, position, Quaternion.identity) as GameObject;
                wallsPositions.Add(position);
            }
        }

        for (int i = 0; i < gridPositions.Count; i++)
          corridorPositions.Add(gridPositions[i]);
    }

    public void SetupScene(int level) //Metoda wywoływana przez GamaManager; Główna metoda skryptu nadzorująca spawnowanie się obiektów
    {
        if (GameObject.FindGameObjectWithTag("Player") == null) Instantiate(Player, new Vector3(0f, 4f, 0f), Quaternion.identity);
        for (int i = 0; i < 5 + (3 * level); i++) //Wzór na ilość pokoi w scenie, w zależności od obecnego poziomu
        {
            int randomDirection = Random.Range(0, 3); //Losowanie kierunku, w którym zespawnuje się następnym pokój
                                                      /*if (oppositeDirection == randomDirection)
                                                      {
                                                          randomDirection = (randomDirection + 1) % 4;
                                                      }*/

            if (posX == 0 && posY == 0) BoardSetup(getX, getY, 0, i); //Na samym początku spawnowany jest standardowy pokój

            // 0-góra ; 2-dół ; 1-prawo ; 3 - lewo;
            //Debug.Log(randomDirection);
            switch (randomDirection) //W zależności od kierunku
            {
                case 0: //góra
                    posX = posX - xAxis / 2 - 1;
                    AlleySpawning(randomDirection, i);
                    break;

                case 2: //dół
                    posX = posX - xAxis / 2 - 1;
                    posY = posY - yAxis;

                    AlleySpawning(randomDirection, i);
                    break;

                case 1: //prawo
                    posY = posY - yAxis / 2 - 1;
                    AlleySpawning(randomDirection, i);
                    break;

                case 3: //lewo
                    posX = posX - xAxis;
                    posY = posY / 2;

                    AlleySpawning(randomDirection, i);
                    break;
            }

            BoardSetup(getX, getY, randomDirection, i + 1); //Następnie spawnowany pokój jest dostosowany do kierunku korytarza              
        }
        
        FillingPlacesOnTheMap();
        DestroyingWalls();
        DestroyingCorridorTiles();

        Instantiate(exit, new Vector3(posX - 1, posY - 1, 0f), Quaternion.identity);
        RandomSpawning(itemTiles, 2, 2 + level, "Chest");


        Clock.turns.ListEnemies();

        posX = posY = getX = getY = 0;
    }
    /************ CZYSZCZENIE MAPY I PRZECHODZENI DO NASTĘPNEGO POZIOMU *************/
    public void ClearingMap(Collider2D other)
    {
        maximumPosition = minimumPosition = Vector3.zero;

        GameObject[] toDestroyFloors = GameObject.FindGameObjectsWithTag("Floor");
        for (int i = 0; i < toDestroyFloors.Length; i++)
            if (toDestroyFloors[i] != null) Destroy(toDestroyFloors[i]);

        GameObject[] toDestroyWalls = GameObject.FindGameObjectsWithTag("Wall");
        for (int i = 0; i < toDestroyWalls.Length; i++)
            if (toDestroyWalls[i] != null) Destroy(toDestroyWalls[i]);

        GameObject[] toDestroyHolder = GameObject.FindGameObjectsWithTag("Holder");
        for (int i = 0; i < toDestroyHolder.Length; i++)
        {
            if (toDestroyHolder[i].name.Contains("Holder")) Destroy(toDestroyHolder[i]);
            else if (toDestroyHolder[i].name.Contains("Room")) Destroy(toDestroyHolder[i]);
        }

        GameObject[] toDestroyEnemies = GameObject.FindGameObjectsWithTag("Enemy");
        for (int i = 0; i < toDestroyEnemies.Length; i++)
            if (toDestroyEnemies[i] != null) Destroy(toDestroyEnemies[i]);

        GameObject[] toDestroyItems = GameObject.FindGameObjectsWithTag("Item");
        for (int i = 0; i < toDestroyItems.Length; i++)
            if (toDestroyItems[i] != null) Destroy(toDestroyItems[i]);

        GameObject[] toDestroySpells = GameObject.FindGameObjectsWithTag("Spell");
        for (int i = 0; i < toDestroySpells.Length; i++)
            if (toDestroySpells[i] != null) Destroy(toDestroySpells[i]);

        GameObject[] toDestroyTraps = GameObject.FindGameObjectsWithTag("Trap");
        for (int i = 0; i < toDestroyTraps.Length; i++)
            if (toDestroyTraps[i] != null) Destroy(toDestroyTraps[i]);

        if (other == null) Destroy(GameObject.Find("Exit(Clone)"));
              else Destroy(other.gameObject); //Niszczenie wyjścia - Exit(Clone)
        //Destroy(GameObject.FindGameObjectWithTag("Player")); //Niszczenie samego gracza
        GameObject.FindGameObjectWithTag("Player").transform.position = new Vector3(0f, 4f, 0f); //Zmiana pozycji gracza. Wymagana przez zmieniony spawn przeciwników
        //Cała mapa powinna zostać wyczyszczona

        //Czyszczenie wszystkich list z wektorami:
        wallsPositions.Clear();
        gridPositions.Clear();
        floorPositions.Clear();
        enemiesPositions.Clear();
        corridorPositions.Clear();
        chestPositions.Clear();

        GameObject.Find("GameManager").GetComponent<TrapsController>().Reset();

        for (int i = 0; i < enemiesPacksSpawned.Length; i++)
            enemiesPacksSpawned[i] = 0;

        // Dodawanie +1 do level jeśli przechodzisz / resetowanie level do 1 jeśli umierasz
        if (!GameObject.Find("GameManager").GetComponent<GameManager>().rip)
            GameObject.Find("GameManager").GetComponent<GameManager>().level++;
        else
        {
            GameObject.Find("GameManager").GetComponent<GameManager>().rip = false;
            GameObject.Find("GameManager").GetComponent<GameManager>().level = 1;

            // Resetart itemów
            ItemController.itemcontroller.type = ItemController.Item.nothing;
            ItemController.itemcontroller.itemSprite.GetComponent<Image>().color = new Color(1f, 1f, 1f, 0f);
            ItemController.itemcontroller.curCooldowns = new int[11];
            ItemController.itemcontroller.enemiesKilledForSoulHunter = 0;

            // Resetart spelli
            SpellController.spellController.type = SpellController.Spell.nothing;            
            SpellController.spellController.spellSprite.GetComponent<Image>().color = new Color(1f, 1f, 1f, 0f);
            SpellController.spellController.curCooldowns = new int[11];

            // Restart statystyk gracza
            Player player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
            player.playerStats.resetStats();
            player.playerStats.refreshStats();
            player.playerStats.dead = false;            
        }

        GameObject.Find("GameManager").GetComponent<GameManager>().InitializeGame();
    }

    /**************** Rzeczy związane ze spawnem przeciwników i przeszkód na mapie ****************/
    [SerializeField] GameObject fire;
    void SpawningFireTrap()
    {
        GameObject instance = null;
        List<Vector3> firePositions = new List<Vector3>();

        Vector3 startPos = new Vector3((int)(Random.Range(gridPositions[0].x + 1, gridPositions[gridPositions.Count - 1].x - 1)),
                            (int)Random.Range(gridPositions[0].y + 1, gridPositions[gridPositions.Count - 1].y - 1), 0f);

        int n = Random.Range(3, 9); //Potrzebna dodatkowa zmienna, aby przekazywać cooldowny do FireControllera tj. ilość spawnowanych pól z ogniem
        for (int i = 0; i < n; i++)
        {
            int position = Random.Range(0, 9);
            
            switch(position)
            {
                case (0): //Lewy dół
                    instance = Instantiate(fire, new Vector3(startPos.x -1f, startPos.y -1f, 0f), Quaternion.identity) as GameObject;
                    break;

                case (1): //Środkowy dół
                    instance = Instantiate(fire, new Vector3(startPos.x, startPos.y -1f, 0f), Quaternion.identity) as GameObject;
                    break;

                case (2): //Prawy dół
                    instance = Instantiate(fire, new Vector3(startPos.x + 1f, startPos.y -1f, 0f), Quaternion.identity) as GameObject;
                    break;

                case (3): //Lewy środek
                    instance = Instantiate(fire, new Vector3(startPos.x -1f, startPos.y, 0f), Quaternion.identity) as GameObject;
                    break;

                case (4): //Środek środek
                    instance = Instantiate(fire, new Vector3(startPos.x, startPos.y, 0f), Quaternion.identity) as GameObject;
                    break;

                case (5): //Prawy środek
                    instance = Instantiate(fire, new Vector3(startPos.x + 1f, startPos.y, 0f), Quaternion.identity) as GameObject;
                    break;

                case (6): //Lewa góra
                    instance = Instantiate(fire, new Vector3(startPos.x -1f, startPos.y + 1f, 0f), Quaternion.identity) as GameObject;
                    break;

                case (7): //Środkowa góra
                    instance = Instantiate(fire, new Vector3(startPos.x, startPos.y + 1f, 0f), Quaternion.identity) as GameObject;
                    break;

                case (8): //Prawa góra
                    instance = Instantiate(fire, new Vector3(startPos.x + 1f, startPos.y + 1f, 0f), Quaternion.identity) as GameObject;
                    break;
            }
            instance.name += i.ToString();
            if (firePositions.Contains(instance.transform.position))
            {
                firePositions.Remove(instance.transform.position);
                Destroy(instance.gameObject);
                i--;
            }
            else if (wallsPositions.Contains(instance.transform.position))
            {
                Destroy(instance.gameObject);
                n--;
            }
            else
            {
                firePositions.Add(instance.transform.position);
                instance.GetComponent<FireController>().increasingCooldown = n;
                enemiesPositions.Add(instance.transform.position);
            }

        }
        Destroy(instance);
    }
}