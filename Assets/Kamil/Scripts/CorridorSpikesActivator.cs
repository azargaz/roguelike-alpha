﻿using UnityEngine;
using System.Collections;

public class CorridorSpikesActivator : MonoBehaviour {

    [SerializeField] public GameObject spikesToActivate;
    int numberOfEntries;
    int curClockNumber;
    bool canBeDestroyed;

    public void Activation()
    {
        if (numberOfEntries >= 1 && canBeDestroyed)
        {
            Destroy(spikesToActivate);
            Destroy(gameObject);
        }

        else if (numberOfEntries == 0)
        {
            numberOfEntries++;
            curClockNumber = Clock.turns.countingPlayerMoves;
        }
    }

    void FixedUpdate()
    {
        if (numberOfEntries > 0 && curClockNumber + 1 < Clock.turns.countingPlayerMoves)
        {
            canBeDestroyed = true;
            spikesToActivate.GetComponent<BoxCollider2D>().enabled = true;
        }
    }
}