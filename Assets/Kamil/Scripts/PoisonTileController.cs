﻿using UnityEngine;
using System.Collections;

public class PoisonTileController : MonoBehaviour {

    int numberOfEntries = 0;
    int currentClock;
    int countingFromRespawnToDestroy;

	void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            gameObject.GetComponent<Collider2D>().enabled = false;
            numberOfEntries++;
            currentClock = Clock.turns.countingPlayerMoves;
        }
    }

    void Awake()
    {
        countingFromRespawnToDestroy = Clock.turns.countingPlayerMoves;
    }

    void FixedUpdate()
    {
        if (numberOfEntries > 0 && currentClock + 1 < Clock.turns.countingPlayerMoves)
        {
            gameObject.GetComponent<Collider2D>().enabled = true;
        }

        if (countingFromRespawnToDestroy + 4 < Clock.turns.countingPlayerMoves)
        {
            Destroy(gameObject);
        }
    }
}