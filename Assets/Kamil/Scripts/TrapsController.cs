﻿using UnityEngine;
using System.Collections;

public class TrapsController : MonoBehaviour
{
    Player player; //skrypt gracza

    Vector3 curPlayerPosition; //Obecna pozycja gracza
    public Collider2D toSave; //Collider kolcó do zapisania
    bool spikesActivator; //Aktywowanie kolców
    int numberOfEntries; //Zmienna do upewnienia się, że gracz otrzyma tylko raz obrażenia

    bool curseActivator; //Aktywowanie klątwy
    int curClockNumber; //Liczba ruchów gracza
    int numberOfEnemies; //Liczba przeciwników znajdujących się na mapie
    [SerializeField] Sprite spikes2;
    [SerializeField] Sprite spikes3;
    [SerializeField] Sprite spikes4;
    /*************************************************************************/
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    void FixedUpdate()
    {
        if(player == null)
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

        if (spikesActivator && curPlayerPosition != player.transform.position && player != null) DesactivatingSpikes();


        if (curseActivator)
        {
            if (curClockNumber < Clock.turns.countingPlayerMoves)
            {
                if (numberOfEnemies != GameObject.FindGameObjectsWithTag("Enemy").Length) curseActivator = false;
                else
                {
                    curClockNumber += 2;
                    player.playerStats.curHealth -= 5f;
                    player.anim.SetTrigger("Curse");
                }
            }
        }

    }

    #region Spikes
    public void ActivatingSpikes(Collider2D other)
    {
        if (numberOfEntries >= 1) { }
        else
        {
            toSave = other;
            other.enabled = false;
            curPlayerPosition = player.transform.position;
            spikesActivator = true;

            if (player.playerStats.curShield > 0) player.playerStats.curShield--; //Najpierw odejmowana jest tarcza
            else player.playerStats.curHealth -= 10; //Potem zdrowia

            // Animacja
            player.anim.SetTrigger("Hit");

            numberOfEntries++;

            if (other.gameObject.GetComponent<SpriteRenderer>().sprite == spikes2) other.gameObject.GetComponent<SpriteRenderer>().sprite = spikes3; //Drugie wejście
            else if (other.gameObject.GetComponent<SpriteRenderer>().sprite == spikes3) other.gameObject.GetComponent<SpriteRenderer>().sprite = spikes4; //Trzecie wejście
            else if (other.gameObject.GetComponent<SpriteRenderer>().sprite == spikes4) { /* Osiągnięto już maksymalny sprite, nic się nie dzieje */ }
            else other.gameObject.GetComponent<SpriteRenderer>().sprite = spikes2; //Pierwsze wejście
        }
    }

    void DesactivatingSpikes()
    {
        if (toSave.transform.position == new Vector3(player.transform.position.x + 1, player.transform.position.y, 0f) ||
        toSave.transform.position == new Vector3(player.transform.position.x - 1, player.transform.position.y, 0f) ||
        toSave.transform.position == new Vector3(player.transform.position.x, player.transform.position.y + 1, 0f) ||
        toSave.transform.position == new Vector3(player.transform.position.x, player.transform.position.y - 1, 0f))
        {
            toSave.enabled = true;
            numberOfEntries = 0;
        }        
    }
    
    #endregion

    #region Curse

    public void Curse()
    {
        curClockNumber = Clock.turns.countingPlayerMoves; //Pobieranie obecnej liczb ruchów
        curseActivator = true; //Aktywowanie klątwy
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        numberOfEnemies = enemies.Length;
    }

    #endregion

    // Do czyszczenia mapy
    public void Reset()
    {
        toSave = null;
        numberOfEntries = 0;
        player = null;
        spikesActivator = false;
    }
}