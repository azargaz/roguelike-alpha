﻿using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class ChestController : MonoBehaviour {
    
    public IEnumerator OpenChest()
    {        
        gameObject.layer = 0;
        anim.SetTrigger("Open");
        Destroy(gameObject, 0.2f);
        yield return new WaitForSeconds(0.1f);
        Instantiate(items[Random.Range(0, items.Length)], gameObject.transform.position, Quaternion.identity);
    }

    /*********************************/
    private Animator anim;
    public GameObject[] items;
    Collider2D collider2d;
    [SerializeField] LayerMask playerMask;

	void Start ()
    {
        collider2d = GetComponent<Collider2D>();
        anim = GetComponent<Animator>();
	}
	/*
	void FixedUpdate ()
    {
        Bounds bounds = collider2d.bounds;

        Vector2 leftSide = new Vector2(bounds.min.x, bounds.max.y / 2);
        Vector2 rightSide = new Vector2(bounds.max.x, bounds.max.y / 2);
        Vector2 topSide = new Vector2(bounds.max.x / 2, bounds.max.y);
        Vector2 downSide = new Vector2(bounds.max.x / 2, bounds.min.y);

        RaycastHit2D leftHit = Physics2D.Raycast(leftSide, Vector3.left, 0.5f, playerMask);
        RaycastHit2D rightHit = Physics2D.Raycast(rightSide, Vector3.right, 0.5f, playerMask);
        RaycastHit2D topHit = Physics2D.Raycast(topSide, -Vector3.down, 0.5f, playerMask);
        RaycastHit2D downHit = Physics2D.Raycast(downSide, Vector3.down, 0.5f, playerMask);

        if (leftHit || rightHit || topHit || downHit)
        {
            Instantiate(items[Random.Range(0, items.Length)], gameObject.transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }*/
}