﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ItemController : MonoBehaviour {

    //Zmienne ogólne:
    private Player player; //Do pobrania obiektu gracza
    private BoardManager boardManager;
    public static ItemController itemcontroller = null;

    public int curCooldown; //Cooldown dla itemów
    public bool isActive; //Sprawdzanie aktywności potrzebne przy niektórych przedmiotach
    [SerializeField] public GameObject itemSprite;
    [SerializeField] private GameObject[] itemSprites;
    [SerializeField] private GameObject[] itemParticles;
    [SerializeField] private GameObject aimForItems;
    /*****************************************************/
    void Start ()
    {
        itemcontroller = this;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        boardManager = GameObject.Find("GameManager").GetComponent<BoardManager>();

        damageToStash = player.playerStats.physicalDamage;

        if (type == Item.nothing) itemSprite.GetComponent<Image>().color = new Color(1f, 1f, 1f, 0f);
        else
        {
            itemSprite.GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);
            itemSprite.GetComponent<Image>().sprite = itemSprites[(int)type].GetComponent<SpriteRenderer>().sprite;
        }
    }
	
	public void Update ()
    {
        if(player == null)
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

        if (Input.GetButtonDown("Fire1") && curCooldown == 0)
        {
            switch (type)
            {
                case (Item.armor): //Zbroja - dodaje 2  armora po użyciu raz na 10 rund
                    player.playerStats.curShield += 2;
                    curCooldown = itemCooldowns[(int)type]; //Cooldown - 10 punktów

                    break;

                case (Item.necromancerMask): //Maska nekromanty - po użyciu, dopóki gracz nie wyjdzie z pokoju każdy przeciwnik zwraca 25hp
                    isActive = true;
                    curCooldown = itemCooldowns[(int)type]; //Cooldown - 3 punkty

                    break;

                case (Item.bomb): //Spawn bomby, którą można potem zdetonować z odległości

                    if (isActive == false)
                    {
                        GameObject spawnedBomb = (GameObject) Instantiate(bombToSpawn, player.transform.position, Quaternion.identity);
                        GameObject clone = (GameObject)Instantiate(itemParticles[(int)type], spawnedBomb.transform.position, Quaternion.identity);
                        clone.SetActive(false);

                        isActive = true;
                        
                        StartCoroutine(BombExplosion(spawnedBomb, clone));

                        curCooldown = itemCooldowns[(int)type]; //Cooldown
                    }
                    break;

                case (Item.shield): //Tarcza dzięki której gracz nie otrzymuje obrażeń od następnego ataku
                    if (isActive == false)
                        isActive = true;
                    //Cooldown znajduję się w graczu w metodzie zliczającej obrażenia zadane graczowi, obecnie 3 punkty
                    break;

                case (Item.midasHand):
                    isActive = true;
                    curCooldown += itemCooldowns[(int)Item.midasHand];
                    break;

                case (Item.lance):
                    if (isActive == false)
                    {
                        Vector3 playerPos = player.transform.position;

                        GameObject[] floors = GameObject.FindGameObjectsWithTag("Floor");
                        int xLimit = 3 + (int)playerPos.x;
                        int yLimit = 3 + (int)playerPos.y;

                        //int xMin = -3 + (int)playerPos.x;
                        //int yMin = -3 + (int)playerPos.y;
                        #region Kolorowanie pól

                        Transform spawnHolderLance = new GameObject("spawnHolderLance").transform;

                        for (int i = 0; i < floors.Length; i++)
                        {
                            if (playerPos == floors[i].transform.position)
                            {
                                GameObject floorToUse = floors[i];

                                if (boardManager.wallsPositions.Contains(playerPos) || !boardManager.floorPositions.Contains(playerPos))
                                { }
                                else
                                {
                                    GameObject instance =
                                    Instantiate(aimForItems, playerPos, Quaternion.identity) as GameObject;

                                    instance.transform.SetParent(spawnHolderLance);
                                }
                                //instance.GetComponent<SpriteRenderer>().color = new Color(1f, 0f, 0f, 1f);

                                //floorToUse.GetComponent<SpriteRenderer>().color = new Color(1f, 0f, 0f, 1f);
                                //floorToUse.GetComponent<SpriteRenderer>().sortingOrder = 1;
                                if (playerPos.x < xLimit) playerPos.x++;

                            }
                        }

                        playerPos.x -= 6;

                        for (int i = 0; i < floors.Length; i++)
                        {
                            if (playerPos == floors[i].transform.position)
                            {
                                GameObject floorToUse = floors[i];
                                if (boardManager.wallsPositions.Contains(playerPos) || !boardManager.floorPositions.Contains(playerPos))
                                { }
                                else
                                {
                                    GameObject instance =
                                    Instantiate(aimForItems, playerPos, Quaternion.identity) as GameObject;

                                    instance.transform.SetParent(spawnHolderLance);
                                }
                                //instance.GetComponent<SpriteRenderer>().color = new Color(1f, 0f, 0f, 1f);
                                if (playerPos.x < xLimit) playerPos.x++;

                            }
                        }
                        playerPos.x -= 3;
                        for (int i = 0; i < floors.Length; i++)
                        {
                            if (playerPos == floors[i].transform.position)
                            {
                                GameObject floorToUse = floors[i];

                                if (boardManager.wallsPositions.Contains(playerPos) || !boardManager.floorPositions.Contains(playerPos))
                                { }
                                else
                                {
                                    GameObject instance =
                                    Instantiate(aimForItems, playerPos, Quaternion.identity) as GameObject;

                                    instance.transform.SetParent(spawnHolderLance);
                                }
                                //instance.GetComponent<SpriteRenderer>().color = new Color(1f, 0f, 0f, 1f);
                                if (playerPos.y < yLimit) playerPos.y++;

                            }
                        }
                        playerPos.y -= 6;
                        for (int i = 0; i < floors.Length; i++)
                        {
                            if (playerPos == floors[i].transform.position)
                            {
                                GameObject floorToUse = floors[i];

                                if (boardManager.wallsPositions.Contains(playerPos) || !boardManager.floorPositions.Contains(playerPos))
                                { }
                                else
                                {
                                    GameObject instance =
                                    Instantiate(aimForItems, playerPos, Quaternion.identity) as GameObject;

                                    instance.transform.SetParent(spawnHolderLance);
                                }
                                //instance.GetComponent<SpriteRenderer>().color = new Color(1f, 0f, 0f, 1f);
                                if (playerPos.y < yLimit) playerPos.y++;

                            }
                        }
                        playerPos.y -= 3;
                        //Debug.Log(playerPos);
                        #endregion

                        isActive = true;
                    }            

                    break;

                case (Item.shovel):

                    bool ifNothingPaintedShovel = false;

                    if (isActive == false)
                    {
                        #region Kolorowanie ścian
                        Vector3 playerPosition = player.transform.position;
                        Transform spawnHolderShovel = new GameObject("spawnHolderShovel").transform;

                        if (boardManager.wallsPositions.Contains(new Vector3(playerPosition.x + 1, playerPosition.y, 0f)))
                        {
                            GameObject instance = Instantiate(aimForItems, new Vector3(playerPosition.x + 1, playerPosition.y, 0f), Quaternion.identity) as GameObject;

                            instance.transform.SetParent(spawnHolderShovel);
                            instance.GetComponent<SpriteRenderer>().color = new Color(1f, 0f, 0f, 1f);
                            ifNothingPaintedShovel = true;
                        }

                        if (boardManager.wallsPositions.Contains(new Vector3(playerPosition.x - 1, playerPosition.y, 0f)))
                        {
                            GameObject instance = Instantiate(aimForItems, new Vector3(playerPosition.x - 1, playerPosition.y, 0f), Quaternion.identity) as GameObject;

                            instance.transform.SetParent(spawnHolderShovel);
                            instance.GetComponent<SpriteRenderer>().color = new Color(1f, 0f, 0f, 1f);
                            ifNothingPaintedShovel = true;
                        }

                        if (boardManager.wallsPositions.Contains(new Vector3(playerPosition.x, playerPosition.y + 1, 0f)))
                        {
                            GameObject instance = Instantiate(aimForItems, new Vector3(playerPosition.x, playerPosition.y + 1, 0f), Quaternion.identity) as GameObject;

                            instance.transform.SetParent(spawnHolderShovel);
                            instance.GetComponent<SpriteRenderer>().color = new Color(1f, 0f, 0f, 1f);
                            ifNothingPaintedShovel = true;
                        }

                        if (boardManager.wallsPositions.Contains(new Vector3(playerPosition.x, playerPosition.y - 1, 0f)))
                        {
                            GameObject instance = Instantiate(aimForItems, new Vector3(playerPosition.x, playerPosition.y - 1, 0f), Quaternion.identity) as GameObject;

                            instance.transform.SetParent(spawnHolderShovel);
                            instance.GetComponent<SpriteRenderer>().color = new Color(1f, 0f, 0f, 1f);
                            ifNothingPaintedShovel = true;
                        }
                        #endregion
                    }
                    isActive = ifNothingPaintedShovel;

                    break;

                case (Item.soulHunter):

                    if (enemiesKilledForSoulHunter >= 25)
                    {
                        player.playerStats.health += 5;
                        player.playerStats.magic += 10;
                        player.playerStats.refreshStats();
                        player.playerStats.curHealth = player.playerStats.maxHealth;

                        enemiesKilledForSoulHunter -= 25;
                    }

                    break;

                case (Item.cursedTalisman):

                    break;

                case (Item.razorBlade):

                    if (player.playerStats.curHealth > (player.playerStats.maxHealth / 4))
                    {
                        player.playerStats.curHealth -= (player.playerStats.maxHealth / 4);
                        player.playerStats.curShield += 1;
                    }
                    else if (player.playerStats.curHealth <= (player.playerStats.maxHealth / 4) && player.playerStats.curHealth > 1)
                    {
                        player.playerStats.curHealth -= player.playerStats.curHealth - 1;
                        player.playerStats.curShield += 1;
                    }
                    else if (player.playerStats.curHealth == 1)
                    {
                        player.playerStats.curHealth -= player.playerStats.curHealth;
                        player.playerStats.curShield += 1;
                    }

                    player.anim.SetTrigger("Hit");

                    break;

                default:

                    break;

            }
        }
        #region Tworzenie boxCollider2D dla lancy i efektów
        if (type == Item.lance && isActive == true && curCooldown == 0)
        {
            GameObject clone = null;

            bool lanceUsed = false;            
            
            if (Input.GetKeyDown(KeyCode.LeftArrow) && !lanceUsed)
            {
                clone = (GameObject)Instantiate(itemParticles[(int)type], player.transform.position, Quaternion.identity);
                clone.transform.Rotate(0, 0, 90);
                Clock.turns.playersTurn = false;
                lanceUsed = true;
            }

            else if (Input.GetKeyDown(KeyCode.RightArrow) && !lanceUsed)
            {
                clone = (GameObject)Instantiate(itemParticles[(int)type], player.transform.position, Quaternion.identity);
                clone.transform.Rotate(0, 0, -90);
                Clock.turns.playersTurn = false;
                lanceUsed = true;
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow) && !lanceUsed)
            {
                clone = (GameObject)Instantiate(itemParticles[(int)type], player.transform.position, Quaternion.identity);
                clone.transform.Rotate(0, 0, 180);
                Clock.turns.playersTurn = false;
                lanceUsed = true;
            }
            else if (Input.GetKeyDown(KeyCode.UpArrow) && !lanceUsed)
            {
                clone = (GameObject)Instantiate(itemParticles[(int)type], player.transform.position, Quaternion.identity);
                clone.transform.Rotate(0, 0, 0);
                Clock.turns.playersTurn = false;
                lanceUsed = true;
            }

            if (lanceUsed)
            {
                StartCoroutine(LanceClearing(clone));
            }            
        }
        #endregion

        #region Sprawdzanie kierunków i niszczenie klocków dla łopaty
        if (type == Item.shovel && isActive == true && curCooldown == 0)
        {
            bool shovelUsed = true;
            GameObject[] wallsToShovel = GameObject.FindGameObjectsWithTag("Wall");
            Vector3 playerPosition = player.transform.position;

            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                for (int i = 0; i < wallsToShovel.Length; i++)
                {
                    if (wallsToShovel[i].transform.position == new Vector3(player.transform.position.x - 1, player.transform.position.y, 0f))
                        Destroy(wallsToShovel[i]);
                }
            }

            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                for (int i = 0; i < wallsToShovel.Length; i++)
                {
                    if (wallsToShovel[i].transform.position == new Vector3(player.transform.position.x + 1, player.transform.position.y, 0f))
                        Destroy(wallsToShovel[i]);
                }
            }

            else if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                for (int i = 0; i < wallsToShovel.Length; i++)
                {
                    if (wallsToShovel[i].transform.position == new Vector3(player.transform.position.x, player.transform.position.y - 1, 0f))
                        Destroy(wallsToShovel[i]);
                }
            }

            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                for (int i = 0; i < wallsToShovel.Length; i++)
                {
                    if (wallsToShovel[i].transform.position == new Vector3(player.transform.position.x, player.transform.position.y + 1, 0f))
                        Destroy(wallsToShovel[i]);
                }
            }
            else shovelUsed = false;

            if (shovelUsed)
             StartCoroutine(ShovelClearing());
        }
        #endregion
    }


    /**************************************************************************/

    void FixedUpdate()
    {
        if ((type == Item.midasHand && curCooldown > 0) || type != Item.midasHand)
        {
            player.midasHandParticles.SetActive(false);
        }

        if (type == Item.cursedTalisman) player.playerStats.physicalDamage = 100;
        else player.playerStats.refreshStats();

        if (type == Item.soulHunter) curCooldown = 25 - enemiesKilledForSoulHunter;

        if (curCooldown < 0) curCooldown = 0;

        if (swappedItems)
        {        
            if (Clock.turns.countingPlayerMoves > curClock + 1)
            {
                itemOnGround.GetComponent<Collider2D>().enabled = true;
                swappedItems = false;
            }         
        }
        //Debug.Log((int)type);
        //Debug.Log(player.playerStats.curHealth);
        //Debug.Log(curCooldown);
    }

    IEnumerator BombExplosion(GameObject spawnedBomb, GameObject bombParticles) //Metoda pomagająca przy wybuchu bomby
    {
        yield return new WaitForSeconds(2f);

        bombParticles.SetActive(true);
        spawnedBomb.GetComponent<Collider2D>().enabled = true;        
        Destroy(bombParticles, 2f);
        
        yield return new WaitForSeconds(0.2f);

        spawnedBomb.GetComponent<Collider2D>().enabled = false;
        Destroy(spawnedBomb);
        isActive = false;
    }

    IEnumerator LanceClearing(GameObject toDestroy)
    {  
        Destroy(toDestroy, 1f);
        isActive = false;
        Destroy(GameObject.Find("spawnHolderLance"));

        yield return new WaitForSeconds(1f);        
        curCooldown = itemCooldowns[(int)type];
    }

    IEnumerator ShovelClearing()
    {
        yield return new WaitForSeconds(0.01f);

        isActive = false;
        Destroy(GameObject.Find("spawnHolderShovel"));
        curCooldown = itemCooldowns[(int)type];
    }

    public void IncreasingCDAfterShield()
    {
        curCooldown = itemCooldowns[(int)type];
    }

    [HideInInspector]public int downArrow;
    [HideInInspector]public int upArrow;
    [HideInInspector]public int leftArrow;
    [HideInInspector]public int rightArrow;

    public void RandomizingMovement()
    {
        downArrow = Random.Range(1, 5);
        upArrow = Random.Range(1, 5);
        leftArrow = Random.Range(1, 5);
        rightArrow = Random.Range(1, 5);

        if (downArrow != leftArrow && upArrow != downArrow && downArrow != rightArrow
            && upArrow != rightArrow && rightArrow != leftArrow && upArrow != leftArrow)
        {
            //Poprawnie przydzielone wartości, wszystkie wartości są różne
        }
        else Invoke("RandomizingMovement", 0.01f);
    }

    public void GettingRandomMovement_CursedTalisman(int number)
    {
        switch(number)
        {
            case (1):
                player.horizontal = -1;
                break;

            case (2):
                player.horizontal = 1;
                break;
                
            case (3):
                player.vertical = -1;
                break;

            case (4):
                player.vertical = 1;
                break;           
        }
    }


    /************************** AKTYWACJA ITEMÓW ETC. **********************/
    //Zmienne dla obsługi itemów:
    //Sprawdzanie, który przedmiot jest aktualnie aktywny:
    public enum Item { nothing, armor, necromancerMask, bomb, shield, midasHand, lance, shovel, soulHunter, cursedTalisman, razorBlade }
    public Item type;
    
    public int[] itemCooldowns = new int[10];
    public int[] curCooldowns = new int[10];

    [SerializeField] public GameObject bombToSpawn; //Sprite bomby, który zostanie zespawnowany
    public int enemiesKilledForSoulHunter; //Ilość zabitych przeciwników od zebrania Łowcy Dusz
    public int hpToPass = 25; //Zmienna pomocnicza do przekazywania maksymalnego zdrowia dla gracza w "Player.cs"
    private float damageToStash; //Pobieranie obrażeń zadawanych przez gracza i zachowywania, aby zwrócić, gdy przedmiotem aktywnym przestanie być Przeklęty Talizman
    public bool extraLife = false; //Dodatkowe życie - domyślnie "false";; Używany w metodzie "KillPlayer()" w Player.cs
    private bool swappedItems;
    private GameObject itemOnGround;
    private int curClock;

    #region Voidy po podniesieniu itemu - pasowałoby przerobić #kiedyś
    void Armor()
    {
        SwitchingItemsOnGround();
        type = Item.armor;
        SwitchingItems();
    }

    void NecromancerMask()
    {
        SwitchingItemsOnGround();
        type = Item.necromancerMask;
        SwitchingItems();      
    }

    void Bomb()
    {
        SwitchingItemsOnGround();
        type = Item.bomb;
        SwitchingItems();
    }

    void Shield()
    {
        SwitchingItemsOnGround();
        type = Item.shield;
        SwitchingItems();
    }

    void MidasHand()
    {
        SwitchingItemsOnGround();
        type = Item.midasHand;
        SwitchingItems();
    }

    void Lance()
    {
        SwitchingItemsOnGround();
        type = Item.lance;
        SwitchingItems();
    }

    void Shovel()
    {
        SwitchingItemsOnGround();
        type = Item.shovel;
        SwitchingItems();
    }

    void SoulHunter()
    {
        SwitchingItemsOnGround();
        type = Item.soulHunter;
        SwitchingItems();
    }

    void CursedTalisman()
    {
        SwitchingItemsOnGround();
        type = Item.cursedTalisman;
        RandomizingMovement();
        SwitchingItems();
    }

    void RazorBlade()
    {
        SwitchingItemsOnGround();
        type = Item.razorBlade;
        SwitchingItems();
    }

    /*************************************/
    void ExtraLife() //Przedmiot pasywny niezwiązany z resztą i niezajmujący slotu na przedmiot
    {
        extraLife = true; //Dostępne jest dodatkowe życie, czyli odrodzenie, gdy pasek spadnie do 0hp
    }
    #endregion
    //****************** Ewentualne funkcje pomocnicze
    public void SwitchingItems()
    {
        //armor = necromantMask = false;
        curCooldown = curCooldowns[(int) type];
        isActive = false;

        if (type == Item.nothing) itemSprite.GetComponent<Image>().color = new Color(1f, 1f, 1f, 0f);
        else
        {                      
            itemSprite.GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);
            itemSprite.GetComponent<Image>().sprite = itemSprites[(int)type].GetComponent<SpriteRenderer>().sprite;
        }
    }

    void SwitchingItemsOnGround()
    {
        if (type == Item.nothing)
            return;
        else
        {
            curCooldowns[(int)type] = curCooldown;
            itemOnGround = (GameObject)Instantiate(itemSprites[(int)type], new Vector3((int)player.transform.position.x, (int)player.transform.position.y, (int)player.transform.position.z), Quaternion.identity);
            itemOnGround.GetComponent<Collider2D>().enabled = false;
            curClock = Clock.turns.countingPlayerMoves;
            swappedItems = true;
        }
    }
}