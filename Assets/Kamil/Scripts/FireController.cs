﻿using UnityEngine;
using System.Collections;

public class FireController : MonoBehaviour
{
    Player player; //skrypt gracza

    Vector3 curPlayerPosition; //Obecna pozycja gracza
    public Collider2D toSave; //Collider kolcó do zapisania
    bool fireActivator; //Aktywowanie ognia
    int numberOfEntries; //Zmienna do upewnienia się, że gracz otrzyma tylko raz obrażenia

    int curClockNumber; //Liczba ruchów gracza
    int curFireCooldown;
    public int increasingCooldown;
    public GameObject fireParticles;
    private GameObject clone;

    [SerializeField] Sprite unactiveSprite;
    [SerializeField] Sprite readySprite;
    [SerializeField] Sprite activeSprite;

    void Start ()
    {
        gameObject.GetComponent<Collider2D>().enabled = false;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        string tempString = gameObject.name;
        tempString = tempString.Replace("Fire(Clone)", "");

        int.TryParse(tempString, out curFireCooldown);
        curClockNumber = Clock.turns.countingPlayerMoves;

        curFireCooldown++;
    }

    void FixedUpdate()
    {
        //if (fireActivator && curPlayerPosition != player.transform.position) DesactivatingFire();

        if (curClockNumber + 1 < Clock.turns.countingPlayerMoves) //Jeśli gracz się poruszył lub wykonał atak
        {
            curFireCooldown--;  //Zmniejszanie cooldownu do wybuchu
            curClockNumber++; //Dostosowywanie wewnętrznego zegara do ruchów gracza

            if (curFireCooldown == 1) gameObject.GetComponent<SpriteRenderer>().sprite = readySprite; //Pola które następnie wybuchnie jest pomarańczowe

            else if (curFireCooldown == 0) //Jeśli wybucha to: 
            {
                fireParticles.SetActive(true);
                curFireCooldown += increasingCooldown; //zwiększa się cooldown zależny od ilości zespawnowanych w tym miejscu pól
                gameObject.GetComponent<SpriteRenderer>().sprite = activeSprite; //Żółty sprite
                gameObject.GetComponent<Collider2D>().enabled = true; //Włączenie collidera
            }
            else //Jeśli pole jest nieaktywne
            {
                fireParticles.SetActive(false);
                Destroy(clone);
                gameObject.GetComponent<SpriteRenderer>().sprite = unactiveSprite; //Brązowy sprite
                gameObject.GetComponent<Collider2D>().enabled = false; //Wyłączenie collidera
                numberOfEntries = 0; //Liczba wejść zerowana - używane do upewnienia się, że gracz otrzyma tylko raz obrażenia
            }
        }
    }

    public void ActivatingFire(Collider2D other)
    {
        if (numberOfEntries >= 1) { } //Sprawdzanie czy gracz nie dostał już obrażeń
        else
        {
            //toSave = other;
            //other.enabled = false;
            //curPlayerPosition = player.transform.position;
            //fireActivator = true;

            if (player.playerStats.curShield > 0) player.playerStats.curShield--; //Najpierw odejmowana jest tarcza
            else player.playerStats.curHealth -= 10; //Potem zdrowia
            player.playerStats.onFire += 8;
            player.anim.SetTrigger("Hit");

            numberOfEntries++; //Gracz już więcej nie otrzyma obrażeń
        }
    }
}